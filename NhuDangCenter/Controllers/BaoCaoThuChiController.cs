﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NhuDangCenter.Models;
using Microsoft.Office.Interop.Excel;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace NhuDangCenter.Controllers
{
    public class BaoCaoThuChiController : Controller
    {
        private NhuDangCenterEntities db = new NhuDangCenterEntities();
        // GET: BaoCaoThuChi
        [Authorize(Roles = "Quan tri")]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize(Roles = "Quan tri")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "TuNgay,DenNgay")] TuNgayDenNgay obj)
        {
            if (ModelState.IsValid)
            {
                var result = db.sp_T_BaoCaoThuChi(obj.TuNgay, obj.DenNgay).OrderByDescending(x =>x.PhanLoai).ToList();
                ViewBag.BangKe = result;
                return View(obj);
            }
            return View();
        }
        public FileResult XuatExcel(DateTime tuNgay, DateTime denNgay)
        {
            var duLieu = db.sp_T_BaoCaoThuChi(tuNgay, denNgay).OrderByDescending(x => x.PhanLoai).ToList();

            //đầu tiên là in tiêu đề
            string tieuDe = "Từ ngày " + tuNgay.ToString("dd/MM/yyyy") + " đến ngày " + denNgay.ToString("dd/MM/yyyy");
                     
            FileResult result = null;
            string server = Server.MapPath("~/App_Data");

            DirectoryInfo outputDir = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments));

            FileInfo templateFile = new FileInfo(server + "//thongkethuchi.xltx");
            FileInfo newFile = new FileInfo(outputDir.FullName + @"\thongkethuchi.xlsx");
            if (newFile.Exists)
            {
                newFile.Delete();  // ensures we create a new workbook
                newFile = new FileInfo(outputDir.FullName + @"\thongkethuchi.xlsx");
            }
            using (ExcelPackage package = new ExcelPackage(newFile, templateFile))
            {
                //Sheet xuất dữ liệu
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];

                //****************************************
                //CODE INSERT THỐNG KÊ THU CHI
                //****************************************
                //Vị trí row sẽ insert:
                //in thống ke
                worksheet.Cells[2, 1].Value = tieuDe;
                worksheet.Cells[2, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[2, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                int row = 4;
                int stt = 1;
                foreach (sp_T_BaoCaoThuChi_Result item in duLieu)
                {
                    worksheet.Cells[row, 1].Value = stt;                    
                    worksheet.Cells[row, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    worksheet.Cells[row, 2].Value = item.PhanLoai;                    
                    worksheet.Cells[row, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    worksheet.Cells[row, 3].Value = item.NoiDung;
                    worksheet.Cells[row, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[row, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    if (item.ThuHocPhi > 0)
                    {
                        worksheet.Cells[row, 4].Value = item.ThuHocPhi;
                        worksheet.Cells[row, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        worksheet.Cells[row, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    }
                    else
                    {
                        worksheet.Cells[row, 4].Value = item.ChiKhac;
                        worksheet.Cells[row, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        worksheet.Cells[row, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    }
                    row++;
                    stt++;
                }

                //Switch the PageLayoutView back to normal
                worksheet.View.PageLayoutView = false;
                // save our new workbook and we are done!
                package.Save();


                string absolutePath = newFile.FullName;// = _environment.WebRootPath + @"\" + relativePath;

                if (System.IO.File.Exists(absolutePath))
                {
                    string fileName = System.IO.Path.GetFileName(absolutePath);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(absolutePath);
                    result = File(fileBytes, "application/x-msdownload", fileName);
                }
                return result;
            }

        }
    }
}