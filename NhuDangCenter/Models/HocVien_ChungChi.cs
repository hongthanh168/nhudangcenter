//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NhuDangCenter.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class HocVien_ChungChi
    {
        public int HocVien_ChungChi_ID { get; set; }
        public Nullable<int> HocVien_ID { get; set; }
        public Nullable<int> ChungChi_ID { get; set; }
        public Nullable<System.DateTime> NgayCap { get; set; }
        public byte[] DinhKem { get; set; }
    
        public virtual ChungChi ChungChi { get; set; }
        public virtual HocVien HocVien { get; set; }
    }
}
