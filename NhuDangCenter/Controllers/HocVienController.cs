﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NhuDangCenter.Models;
using System.Web.UI.WebControls;
using System.Text;

namespace NhuDangCenter.Controllers
{
    public class HocVienController : Controller
    {
        private NhuDangCenterEntities db = new NhuDangCenterEntities();

        // GET: HocVien
        public ActionResult Index()
        {
            var hocViens = db.HocViens.Include(h => h.TinhTrang);
            return View(hocViens.ToList());
        }

        // GET: HocVien/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HocVien hocVien = db.HocViens.Find(id);
            if (hocVien == null)
            {
                return HttpNotFound();
            }
            return View(hocVien);
        }

        // GET: HocVien/Create
        //[Authorize(Roles = "TruongBan")]
        public ActionResult Create()
        {
            ViewBag.TinhTrang_ID = new SelectList(db.TinhTrangs, "TinhTrang_ID", "TinhTrang_Name");
            return PartialView();
        }

        // POST: HocVien/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HocVien_ID,HocVien_Name,GioiTinh,NgaySinh,DiaChi,DienThoai,Email,NgayTaoHocVien,TinhTrang_ID,HinhAnh")] HocVien hocVien, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    byte[] imageBytes = null;
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        imageBytes = reader.ReadBytes(upload.ContentLength);
                    }
                    hocVien.HinhAnh = imageBytes;
                }
                if (hocVien.NgayTaoHocVien==null)
                {
                    hocVien.NgayTaoHocVien = DateTime.Now;
                }
                //Lấy mã học viên tự động
                hocVien.MaHocVien =Convert.ToString( db.store_TaoMaHocVien(DateTime.Now.Year).First()); 
                db.HocViens.Add(hocVien);
                db.SaveChanges();
                return RedirectToAction("Details", new { id = hocVien.HocVien_ID });
                //return RedirectToAction("Index");
                //return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }

            ViewBag.TinhTrang_ID = new SelectList(db.TinhTrangs, "TinhTrang_ID", "TinhTrang_Name", hocVien.TinhTrang_ID);
            return PartialView(hocVien);
        }

        // GET: HocVien/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HocVien hocVien = db.HocViens.Find(id);
            if (hocVien == null)
            {
                return HttpNotFound();
            }
            ViewBag.TinhTrang_ID = new SelectList(db.TinhTrangs, "TinhTrang_ID", "TinhTrang_Name", hocVien.TinhTrang_ID);
            return View(hocVien);
        }

        // POST: HocVien/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HocVien_ID,HocVien_Name,GioiTinh,NgaySinh,DiaChi,DienThoai,Email,NgayTaoHocVien,TinhTrang_ID")] HocVien hocVien, Boolean IsChangePhoto, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    byte[] imageBytes = null;
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        imageBytes = reader.ReadBytes(upload.ContentLength);
                    }
                    hocVien.HinhAnh = imageBytes;
                }
                else
                {
                    if (!IsChangePhoto)
                    {
                        //tìm lại photo cũ
                        var q = from temp in db.HocViens where temp.HocVien_ID == hocVien.HocVien_ID select temp.HinhAnh;
                        byte[] cover = q.First();
                        hocVien.HinhAnh = cover;
                    }
                }
               db.Entry(hocVien).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", new { id = hocVien.HocVien_ID });
            }
            ViewBag.TinhTrang_ID = new SelectList(db.TinhTrangs, "TinhTrang_ID", "TinhTrang_Name", hocVien.TinhTrang_ID);
            return View(hocVien);
        }

        // GET: HocVien/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HocVien hocVien = db.HocViens.Find(id);
            if (hocVien == null)
            {
                return HttpNotFound();
            }
            return View(hocVien);
        }

        // POST: HocVien/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            HocVien hocVien = db.HocViens.Find(id);
            db.HocViens.Remove(hocVien);
            //THÔNG BÁO HỌC VIÊN CÓ TRONG DANH SÁCH LỚP - KHÔNG XÓA ĐƯỢC
            
            try
            {
                ViewBag.thongbao = "";

                db.SaveChanges();
            }
            catch (Exception)
            {
                ViewBag.thongbao = "Học viên trong danh sách lớp học - không xóa được";
                return View(hocVien);
            }
           
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
