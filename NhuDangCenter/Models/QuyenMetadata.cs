﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace NhuDangCenter.Models
{

    public partial class LopMetadata
    {
        public int Lop_ID { get; set; }
        [Display(Name = "Mã")]
        public string MaLop { get; set; }
        [Display(Name = "Tên")]
        public string Lop_Name { get; set; }
        [Display(Name = "Thứ")]
        public Nullable<int> Thu_ID { get; set; }
        [Display(Name = "Ca")]
        public Nullable<int> Ca_ID { get; set; }
        [Display(Name = "Cấp độ")]
        public Nullable<int> CapDo_ID { get; set; }
        [Display(Name = "B.đầu")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> BatDau { get; set; }
        [Display(Name = "K.Thúc")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> KetThuc { get; set; }
        [Display(Name = "T.gian")]
        public string ThoiGianLop { get; set; }
        [Display(Name = "H.Phí")]
        public Nullable<int> HocPhi { get; set; }
        [Display(Name = "G.Viên")]
        public Nullable<int> GiaoVien_ID { get; set; }
        [Display(Name = "T.Giảng")]
        public Nullable<int> TroGiang_ID { get; set; }
        [Display(Name = "Phòng")]
        public Nullable<int> Phong_ID { get; set; }
        [Display(Name = "T.Trạng")]
        public Nullable<int> TinhTrangLop_ID { get; set; }



    }
    [MetadataType(typeof(LopMetadata))]
    public partial class Lop
    {

    }

    public partial class HocVienMetadata
    {


        public int HocVien_ID { get; set; }
        [Display(Name = "Họ tên")]
        public string HocVien_Name { get; set; }
        [Display(Name = "Giới tính")]
        [UIHint("GioiTinh")]
        public Nullable<bool> GioiTinh { get; set; }
        [Display(Name = "Ngày sinh")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> NgaySinh { get; set; }
        [Display(Name = "Địa chỉ")]
        public string DiaChi { get; set; }
        [Display(Name = "Đ.Thoại")]
        public string DienThoai { get; set; }
        
        public string Email { get; set; }
        [Display(Name = "Ngày tạo")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> NgayTaoHocVien { get; set; }
        [Display(Name = "T.Trạng")]
        public Nullable<int> TinhTrang_ID { get; set; }
        [Display(Name = "Hình ảnh")]
        public byte[] HinhAnh { get; set; }

    }
    [MetadataType(typeof(HocVienMetadata))]
    public partial class HocVien
    {

    }

    public partial class CTHocPhiMetadata
    {
        public int CTHocVien_ID { get; set; }
        public int STT { get; set; }
        [Display(Name = "Ngày đóng tiền")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> NgayDongTien { get; set; }
        [Display(Name = "Số tiền")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public Nullable<int> SoTien { get; set; }
        [Display(Name = "Miễn giảm")]
        public Nullable<int> MienGiam { get; set; }
        [Display(Name = "Phải nộp")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public Nullable<int> ConLai { get; set; }
        [Display(Name = "Nội dung")]
        public string NoiDung { get; set; }
        [Display(Name = "Lý do miễn giảm")]
        public string LyDoMienGiam { get; set; }

       
    }
    [MetadataType(typeof(CTHocPhiMetadata))]
    public partial class CTHocPhi
    {

    }
    public partial class HocVien_ChungChiMetadata
    {
        public int HocVien_ChungChi_ID { get; set; }
        [Display(Name = "Học viên")]
        public Nullable<int> HocVien_ID { get; set; }
        [Display(Name = "Chứng chỉ")]
        public Nullable<int> ChungChi_ID { get; set; }
        [Display(Name = "Ngày cấp")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> NgayCap { get; set; }
        [Display(Name = "Đính kèm")]
        public byte[] DinhKem { get; set; }

        public virtual ChungChi ChungChi { get; set; }
        public virtual HocVien HocVien { get; set; }
    }
    [MetadataType(typeof(HocVien_ChungChiMetadata))]
    public partial class HocVien_ChungChi
    {

    }
    public partial class GiaoVienMetadata
    {
        

        public int GiaoVien_ID { get; set; }
        [Display(Name = "Họ tên")]
        public string GiaoVien_Name { get; set; }
        [Display(Name = "Giới tính")]
        [UIHint("GioiTinh")]
        public Nullable<bool> GioiTinh { get; set; }
        [Display(Name = "Ngày sinh")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> NgaySinh { get; set; }
        [Display(Name = "Địa chỉ")]
        public string DiaChi { get; set; }
        [Display(Name = "Điện thoại")]
        public string DienThoai { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Trình độ")]
        public Nullable<int> TrinhDo_ID { get; set; }
        [UIHint("MyBoolean")]
        public Nullable<bool> IsGiaoVien { get; set; }
        [Display(Name = "Lương chính")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public Nullable<int> LuongChinh { get; set; }
        [Display(Name = "Lương trợ giảng")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public Nullable<int> LuongTroGiang { get; set; }

      
    }
    [MetadataType(typeof(GiaoVienMetadata))]
    public partial class GiaoVien
    {

    }
    public partial class store_ThuHocPhi_ResultMetadata
    {
        public int CTHocVien_ID { get; set; }
        [Display(Name = "Học viên")]
        public string HocVien_Name { get; set; }
        [Display(Name = "Học phí")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public int HocPhi { get; set; }
        public int Lop_ID { get; set; }
        [Display(Name = "Lớp")]
        public string Lop_Name { get; set; }
    }
    [MetadataType(typeof(store_ThuHocPhi_ResultMetadata))]
    public partial class store_ThuHocPhi_Result
    {

    }
    public partial class store_NoHocPhi_ResultMetadata
    {
        public string HocVien_Name { get; set; }
        [Display(Name = "Học phí")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public int HocPhi { get; set; }
        [Display(Name = "Đã nộp")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public int DaNop { get; set; }
        [Display(Name = "Còn nợ")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public int ConNo { get; set; }
        public int Lop_ID { get; set; }
        [Display(Name = "Lớp")]
        public string Lop_Name { get; set; }
    }
    [MetadataType(typeof(store_NoHocPhi_ResultMetadata))]
    public partial class store_NoHocPhi_Result
    {

    }
}