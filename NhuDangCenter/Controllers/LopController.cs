﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NhuDangCenter.Models;

namespace NhuDangCenter.Controllers
{
    public class LopController : Controller
    {
        private NhuDangCenterEntities db = new NhuDangCenterEntities();

        // GET: Lops
        public ActionResult Index()
        {
            var lops = db.Lops.Include(l => l.Ca).Include(l => l.CapDo).Include(l => l.GiaoVien).Include(l => l.Phong).Include(l => l.Thu).Include(l => l.TinhTrangLop);
                 
            return View(lops.ToList());
        }
        // GET: Lops
        public ActionResult ThoiKhoaBieu()
        {
            //Lấy danh sách thứ
            List<Thu> thuList = db.Thus.ToList();
            ViewBag.Thus = thuList;
            var lops = db.store_ThoiKhoaBieu();// db.Lops.Include(l => l.Ca).Include(l => l.CapDo).Include(l => l.GiaoVien).Include(l => l.Phong).Include(l => l.Thu).Include(l => l.TinhTrangLop);

            return View(lops.ToList());
        }
        // GET: Lops
        public ActionResult ThoiKhoaBieu_khoi()
        {
            //Lấy danh sách thứ
            List<string> thuList = new List<string>();
            thuList.Add("2");
            thuList.Add("3");
            thuList.Add("4");
            thuList.Add("5");
            thuList.Add("6");
            thuList.Add("7");
            

            ViewBag.Thus = thuList;
            List<Phong> phongList = db.Phongs.ToList();
            ViewBag.Phongs = phongList.OrderBy(p=>p.Phong_Name);
            List<Ca> caList = db.Cas.ToList();
            ViewBag.Cas = caList;
            var lops = db.store_ThoiKhoaBieu();// db.Lops.Include(l => l.Ca).Include(l => l.CapDo).Include(l => l.GiaoVien).Include(l => l.Phong).Include(l => l.Thu).Include(l => l.TinhTrangLop);

            return View(lops.ToList());
        }
        public ActionResult Index1()
        {
            var lops = db.Lops.Include(l => l.Ca).Include(l => l.CapDo).Include(l => l.GiaoVien).Include(l => l.Phong).Include(l => l.Thu).Include(l => l.TinhTrangLop);

            return View(lops.ToList());
        }
        //set cache để nó load lại cái mới cập nhật
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult LopDuKien()
        {
            var lops = db.Lops.Include(l => l.Ca).Include(l => l.CapDo).Include(l => l.GiaoVien).Include(l => l.Phong).Include(l => l.Thu).Include(l => l.TinhTrangLop)
                .Where(l=>l.BatDau > DateTime.Now) ;
            return PartialView("_LopPartial", lops.ToList());
        }
        public ActionResult LopDangMo()
        {
            var lops = db.Lops.Include(l => l.Ca).Include(l => l.CapDo).Include(l => l.GiaoVien).Include(l => l.Phong).Include(l => l.Thu).Include(l => l.TinhTrangLop)
                .Where(l => l.KetThuc > DateTime.Now && l.BatDau <= DateTime.Now);
            return PartialView("_LopPartial", lops.ToList());
        }
        public ActionResult LopKetThuc()
        {
            var lops = db.Lops.Include(l => l.Ca).Include(l => l.CapDo).Include(l => l.GiaoVien).Include(l => l.Phong).Include(l => l.Thu).Include(l => l.TinhTrangLop)
                .Where(l => l.KetThuc < DateTime.Now);
            return PartialView("_LopPartial", lops.ToList());
        }
        public ActionResult dsLop()
        {
           
            var lops = db.Lops.Include(l => l.Ca).Include(l => l.CapDo).Include(l => l.GiaoVien).Include(l => l.Phong).Include(l => l.Thu).Include(l => l.TinhTrangLop);

            return PartialView(lops.ToList());
        }
       
        public ActionResult dsDiemHocVien(int LopID)
        {
            var cTHocViens = db.CTHocViens.Include(c => c.HocVien).Where(c => c.Lop_ID == LopID);
           // var cTHocViens = db.CTHocViens.Include(c => c.HocVien).Include(c => c.Lop);
            return PartialView(cTHocViens.ToList());
        }
        //Danh sách không thuộc lớp
        public ActionResult dsHocVien0(int LopID)
        {
            //lấy ra danh sách học viên không thuộc lớp đang chọn
            ViewBag.TenLop = db.Lops.Where(l=>l.Lop_ID==LopID).Select(l=>l.Lop_Name).First();
            return PartialView(db.store_Lop_HocVien(LopID).ToList());
             
        }
        //Danh sách trong lớp
        public ActionResult dsHocVien1(int LopID)
        {
            //lấy ra danh sách học viên không thuộc lớp đang chọn
            ViewBag.TenLop = db.Lops.Where(l => l.Lop_ID == LopID).Select(l => l.Lop_Name).First();
            return PartialView(db.store_Lop_HocVien1(LopID).ToList());

        }

        public ActionResult InsertHV(int HocVienID, int LopID)
        {
            db.InsertHocVien_Lop(HocVienID,LopID);
            db.SaveChanges();
            //lấy lại dữ liệu
            return PartialView("dsDiemHocVien", db.CTHocViens.Include(c => c.HocVien).Where(c => c.Lop_ID==LopID));
        }
        //Chuyển học viên từ lớp cũ sang lớp mới
        public ActionResult ChuyenHV(int HocVienID, int LopcuID, int LopmoiID)
        {
            db.store_chuyenHocVien_Lop(LopcuID,LopmoiID,HocVienID);
            db.SaveChanges();
            //lấy lại dữ liệu
            return PartialView("dsDiemHocVien", db.CTHocViens.Include(c => c.HocVien).Where(c => c.Lop_ID == LopcuID));
        }

        public ActionResult dieuKienLoc(int LopID, int HocVienID)
        {
       
            //lấy ra danh sách lớp
            var tv = db.Lops.Where(x => x.Lop_ID != LopID).ToList();
            if (tv == null)
            {
                ViewBag.SelectedPos = -1;
            }
            else
            {
                ViewBag.SelectedPos = LopID;
            }
            return PartialView(tv);
        }
        // GET: Lops/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lop lop = db.Lops.Find(id);
            if (lop == null)
            {
                return HttpNotFound();
            }
            return PartialView(lop);
        }

        // GET: Lops/Details/5
        public ActionResult DetailsLop_HocVien()
        {

            var lops = db.Lops.Include(l => l.Ca).Include(l => l.CapDo).Include(l => l.GiaoVien).Include(l => l.Phong).Include(l => l.Thu).Include(l => l.TinhTrangLop);

            return View(lops.ToList());
        }

       
        // GET: Lops/Create
        public ActionResult Create()
        {
            ViewBag.Ca_ID = new SelectList(db.Cas, "Ca_ID", "Ca_Name");
            ViewBag.CapDo_ID = new SelectList(db.CapDoes, "CapDo_ID", "CapDo_Name");
            ViewBag.GiaoVien_ID = new SelectList(db.GiaoViens, "GiaoVien_ID", "GiaoVien_Name");

            //TRợ giảng có thể không có
            List<SelectListItem> list = new List<SelectListItem>();
            var newItem = new SelectListItem { Text = "(Không chọn)", Value = "" };
            list.Add(newItem);
            var query = (from k in db.GiaoViens                         
                         select new SelectListItem { Text = k.GiaoVien_Name, Value = k.GiaoVien_ID.ToString() }).Distinct();
            list.AddRange(query.ToList());
            ViewBag.TroGiang_ID = list;

            ViewBag.Phong_ID = new SelectList(db.Phongs, "Phong_ID", "Phong_Name");
            ViewBag.Thu_ID = new SelectList(db.Thus, "Thu_ID", "Thu_Name");
            ViewBag.TinhTrangLop_ID = new SelectList(db.TinhTrangLops, "TinhTrangLop_ID", "TinhTrangLop_Name");
            return PartialView();
        }

        // POST: Lops/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Lop_ID,MaLop,Lop_Name,Thu_ID,Ca_ID,CapDo_ID,BatDau,KetThuc,ThoiGianLop,HocPhi,GiaoVien_ID,TroGiang_ID,Phong_ID,TinhTrangLop_ID,LuongTroGiang, LuongGiangChinh")] Lop lop)
        {
            if (ModelState.IsValid)
            {
                db.Lops.Add(lop);
                db.SaveChanges();
                //return RedirectToAction("Index");
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Index", "Lop");
                //return Json(new { result = "Redirect", url = Url.Action("Index", "Lop") });
            }

            ViewBag.Ca_ID = new SelectList(db.Cas, "Ca_ID", "Ca_Name", lop.Ca_ID);
            ViewBag.CapDo_ID = new SelectList(db.CapDoes, "CapDo_ID", "CapDo_Name", lop.CapDo_ID);
            ViewBag.TroGiang_ID = new SelectList(db.GiaoViens, "GiaoVien_ID", "GiaoVien_Name");
            ViewBag.GiaoVien_ID = new SelectList(db.GiaoViens, "GiaoVien_ID", "GiaoVien_Name", lop.GiaoVien_ID);
            ViewBag.Phong_ID = new SelectList(db.Phongs, "Phong_ID", "Phong_Name", lop.Phong_ID);
            ViewBag.Thu_ID = new SelectList(db.Thus, "Thu_ID", "Thu_Name", lop.Thu_ID);
            ViewBag.TinhTrangLop_ID = new SelectList(db.TinhTrangLops, "TinhTrangLop_ID", "TinhTrangLop_Name", lop.TinhTrangLop_ID);
            return PartialView(lop);
        }

        // GET: Lops/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lop lop = db.Lops.Find(id);
            if (lop == null)
            {
                return HttpNotFound();
            }
            ViewBag.Ca_ID = new SelectList(db.Cas, "Ca_ID", "Ca_Name", lop.Ca_ID);
            ViewBag.CapDo_ID = new SelectList(db.CapDoes, "CapDo_ID", "CapDo_Name", lop.CapDo_ID);

            //TRợ giảng có thể không có
            List<SelectListItem> list = new List<SelectListItem>();
            if (lop.TroGiang_ID.HasValue == false)
            {
                list.Add(new SelectListItem { Text = "(Không chọn)", Value = "", Selected=true});
            }
            else
            {
                list.Add(new SelectListItem { Text = "(Không chọn)", Value = "" });
            }
            foreach (var item in db.GiaoViens)
            {
                Boolean isSelected = false;
                if (item.GiaoVien_ID == lop.TroGiang_ID)
                {
                    isSelected = true;
                }
                list.Add(new SelectListItem { Text = item.GiaoVien_Name, Value = item.GiaoVien_ID.ToString(), Selected = isSelected });
            }
            ViewBag.TroGiang_ID = list;

            ViewBag.GiaoVien_ID = new SelectList(db.GiaoViens, "GiaoVien_ID", "GiaoVien_Name", lop.GiaoVien_ID);
            ViewBag.Phong_ID = new SelectList(db.Phongs, "Phong_ID", "Phong_Name", lop.Phong_ID);
            ViewBag.Thu_ID = new SelectList(db.Thus, "Thu_ID", "Thu_Name", lop.Thu_ID);
            ViewBag.TinhTrangLop_ID = new SelectList(db.TinhTrangLops, "TinhTrangLop_ID", "TinhTrangLop_Name", lop.TinhTrangLop_ID);
            return PartialView(lop);
        }

        // POST: Lops/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Lop_ID,MaLop,Lop_Name,Thu_ID,Ca_ID,CapDo_ID,BatDau,KetThuc,ThoiGianLop,HocPhi,GiaoVien_ID,TroGiang_ID,Phong_ID,TinhTrangLop_ID,,LuongTroGiang, LuongGiangChinh")] Lop lop)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lop).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            ViewBag.Ca_ID = new SelectList(db.Cas, "Ca_ID", "Ca_Name", lop.Ca_ID);
            ViewBag.CapDo_ID = new SelectList(db.CapDoes, "CapDo_ID", "CapDo_Name", lop.CapDo_ID);
            ViewBag.GiaoVien_ID = new SelectList(db.GiaoViens, "GiaoVien_ID", "GiaoVien_Name", lop.GiaoVien_ID);
            ViewBag.Phong_ID = new SelectList(db.Phongs, "Phong_ID", "Phong_Name", lop.Phong_ID);
            ViewBag.Thu_ID = new SelectList(db.Thus, "Thu_ID", "Thu_Name", lop.Thu_ID);
            ViewBag.TinhTrangLop_ID = new SelectList(db.TinhTrangLops, "TinhTrangLop_ID", "TinhTrangLop_Name", lop.TinhTrangLop_ID);
            return PartialView(lop);
        }

        // GET: Lops/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lop lop = db.Lops.Find(id);
            if (lop == null)
            {
                return HttpNotFound();
            }
            return PartialView(lop);
        }

        // POST: Lops/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Lop lop = db.Lops.Find(id);
            db.Lops.Remove(lop);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
