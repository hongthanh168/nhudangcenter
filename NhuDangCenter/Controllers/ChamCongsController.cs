﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NhuDangCenter.Models;

namespace NhuDangCenter.Controllers
{
    public class ChamCongsController : Controller
    {
        private NhuDangCenterEntities db = new NhuDangCenterEntities();

        // GET: ChamCongs
        public ActionResult Index(int? thang, int? nam)
        {
            ViewBag.TinhTrang = 0;
            if (thang != null && nam != null) //người dùng có chọn năm và tháng
            {
                ViewBag.Thang = thang;
                ViewBag.Nam = nam;
               var bang = db.ChamCongs.Where(h => h.NgayChamCong.Month==thang.Value && h.NgayChamCong.Year==nam.Value).FirstOrDefault();
                if (bang == null) //nếu chưa có bảng phân ca
                {
                    ViewBag.TinhTrang = 1;
                }
                else //đã có bảng phân ca
                {
                    ViewBag.TinhTrang = 2;
                    ViewBag.BangPhanCa = bang;

                    //trả về danh sách các ngày làm việc, dưới dạng mảng số
                    List<DateTime> ngayLamViec = LayDanhSachNgayLamViec(thang.Value, nam.Value);
                    ViewBag.NgayLamViec = ngayLamViec;
                                  
                    //tìm bảng phân ca cho ngày đầu tiên của tháng là ca đầu tiên trong ngày                    
                    return View();
                }
            }
            return View();
        }
        public ActionResult PhanCaTuDong(int thang, int nam)
        {
            int ngayKetThuc = 31;
            if (thang == 4 || thang == 6 || thang == 9 || thang == 11)
            {
                ngayKetThuc = 30;
            }
            if (thang == 2)
            {
                if ((nam % 4 == 0 && nam % 100 != 0) || (nam % 400 == 0))
                {
                    ngayKetThuc = 29;
                }
                else
                {
                    ngayKetThuc = 28;
                }
            }
            DateTime ngayDau = new DateTime(nam, thang, 1);
            DateTime ngayCuoi = new DateTime(nam, thang, ngayKetThuc);
            db.sp_T_TaoBangChamCong(ngayDau, ngayCuoi);
            return RedirectToAction("Index", new { thang = thang, nam = nam });
        }
        public List<DateTime> LayDanhSachNgayLamViec(int thang, int nam)
        {
            //Array ngayLamViec = db.ChamCongs.Where(t => t.NgayChamCong.Year == nam && t.NgayChamCong.Month == thang).Select(t => t.NgayChamCong.Day).Distinct().ToArray();
            List<DateTime> ngayLamViec = new List<DateTime>();
            int ngayKetThuc = 31;
            if (thang == 4 || thang == 6 || thang == 9 || thang == 11)
            {
                ngayKetThuc = 30;
            }
            if (thang == 2)
            {
                if ((nam % 4 == 0 && nam % 100 != 0) || (nam % 400 == 0))
                {
                    ngayKetThuc = 29;
                }
                else
                {
                    ngayKetThuc = 28;
                }
            }
            for(int i =1; i <= ngayKetThuc; i++)
            {
                ngayLamViec.Add(new DateTime(nam, thang, i));
            }
            return ngayLamViec;
        }
        public ActionResult LayPhanCaNgay(int ngay, int thang, int nam)
        {
            DateTime ngayChamCong = new DateTime(nam, thang, ngay);
            IEnumerable<sp_T_LayBangChamCongNgay_Result> chamCong = db.sp_T_LayBangChamCongNgay(ngayChamCong).ToList();
            if (chamCong == null)
            {
                return HttpNotFound();
            }
            //xử lý dữ liệu
            //lấy danh sách các ca
            IEnumerable<Ca> dsCa = db.Cas;
            //Dictionary<int, List<string>> dsGiaovienCa = new Dictionary<int, List<string>>();
            List<int> listNVID = chamCong.Select(x => x.GiaoVien_ID).Distinct().ToList();
            List<GiaoVien> dsGiaoVien = new List<GiaoVien>();
            foreach (int id in listNVID)
            {
                dsGiaoVien.Add(db.GiaoViens.Find(id));                
            }
            ViewBag.Ca = dsCa;
            ViewBag.GiaoVien = dsGiaoVien;
            ViewBag.NgayHienTai = ngayChamCong;
            return PartialView("PhanCaNgay", chamCong);
        }
        public ActionResult NghiCaNgay(DateTime ngay)
        {
            ChamCong cc = new ChamCong();
            cc.NgayChamCong = ngay;
            return PartialView(cc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NghiCaNgay([Bind(Include ="NgayChamCong")]ChamCong cc)
        {
            IEnumerable<ChamCong> chamCongs = db.ChamCongs.Where(x => x.NgayChamCong == cc.NgayChamCong);
            foreach (ChamCong item in chamCongs)
            {
                item.KHChamCong_ID = 2; //Nghỉ
                                        //item.GiaoVienDayBu_ID = null;
                db.Entry(item).State = EntityState.Modified;
            }
            db.SaveChanges();
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        // GET: ChamCongs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChamCong chamCong = db.ChamCongs.Find(id);
            if (chamCong == null)
            {
                return HttpNotFound();
            }
            return View(chamCong);
        }
        public ActionResult ChamCongNgay()
        {
            return View();            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChamCongNgay(string ngay)
        {
            DateTime ngayChamCong = DateTime.ParseExact(ngay, "d/M/yyyy", null);
            IEnumerable<sp_T_LayBangChamCongNgay_Result> chamCong = db.sp_T_LayBangChamCongNgay(ngayChamCong).ToList();
            if (chamCong == null)
            {
                return HttpNotFound();
            }
            //xử lý dữ liệu
            //lấy danh sách các ca
            IEnumerable<Ca> dsCa = db.Cas;
            Dictionary<int, List<string>> dsGiaovienCa = new Dictionary<int, List<string>>();
            List<int> listNVID = chamCong.Select(x =>x.GiaoVien_ID).Distinct().ToList();
            List<GiaoVien> dsGiaoVien = new List<GiaoVien>();
            foreach (int id in listNVID)
            {
                dsGiaoVien.Add(db.GiaoViens.Find(id));
                List<string> kq = new List<string>();
                foreach (Ca c in dsCa)
                {
                    sp_T_LayBangChamCongNgay_Result item = chamCong.Where(x => x.Ca_ID == c.Ca_ID && x.GiaoVien_ID==id).FirstOrDefault();
                    if (item!=null)
                    {
                        kq.Add(item.VaiTro.ToString() + "-" + item.Lop_Name);
                    }
                    else
                    {
                        kq.Add("-");
                    }                    
                }
                dsGiaovienCa.Add(id, kq);
            }            
            ViewBag.Ca = dsCa;
            ViewBag.GiaoVien = dsGiaoVien; 
            ViewBag.GiaoVienCa = dsGiaovienCa;
            return View(chamCong);
        }
        // GET: ChamCongs/Create
        public ActionResult Create(DateTime? ngay)
        {
            ChamCong chamCong = new ChamCong();
            if (ngay.HasValue)
            {
                chamCong.NgayChamCong = ngay.Value;
            }
            else
            {
                chamCong.NgayChamCong = DateTime.Now;
            }
            var vaitros = new SelectList(
                new[]
                {
                    new { VaiTro = 0, Name = "Trợ giảng" },
                    new { VaiTro = 1, Name = "Dạy chính" }
                },
                "VaiTro",
                "Name"
            );
            ViewBag.VaiTro = vaitros;
            ViewBag.Lop_ID = new SelectList(db.Lops, "Lop_ID", "Lop_Name");
            ViewBag.KHChamCong_ID = new SelectList(db.KHChamCongs, "KHChamCong_ID", "KHChamCong_Name");
            ViewBag.GiaoVien_ID = new SelectList(db.GiaoViens, "GiaoVien_ID", "GiaoVien_Name");
            return PartialView(chamCong);
        }

        // POST: ChamCongs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ChamCong_ID,NgayChamCong,Lop_ID,GiaoVien_ID,KHChamCong_ID,GiaoVienDayBu_ID,VaiTro")] ChamCong chamCong)
        {
            if (ModelState.IsValid)
            {
                db.ChamCongs.Add(chamCong);
                db.SaveChanges();
                return Json(new { success=true}, JsonRequestBehavior.AllowGet);
            }
            var vaitros = new SelectList(
                new[]
                {
                    new { VaiTro = 0, Name = "Trợ giảng" },
                    new { VaiTro = 1, Name = "Dạy chính" }
                },
                "VaiTro",
                "Name"
            );
            ViewBag.VaiTro = vaitros;
            ViewBag.Lop_ID = new SelectList(db.Lops, "Lop_ID", "Lop_Name");
            ViewBag.KHChamCong_ID = new SelectList(db.KHChamCongs, "KHChamCong_ID", "KHChamCong_Name");
            ViewBag.GiaoVien_ID = new SelectList(db.GiaoViens, "GiaoVien_ID", "GiaoVien_Name");
            return PartialView(chamCong);
        }

        // GET: ChamCongs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChamCong chamCong = db.ChamCongs.Find(id);
            if (chamCong == null)
            {
                return HttpNotFound();
            }
            var vaitros = new SelectList(
                new[]
                {
                    new { VaiTro = 0, Name = "Trợ giảng" },
                    new { VaiTro = 1, Name = "Dạy chính" }
                },
                "VaiTro",
                "Name",
                chamCong.KHChamCong_ID
            );
            ViewBag.VaiTro = vaitros;
            ViewBag.Lop_ID = new SelectList(db.Lops, "Lop_ID", "Lop_Name", chamCong.Lop_ID);
            ViewBag.KHChamCong_ID = new SelectList(db.KHChamCongs, "KHChamCong_ID", "KHChamCong_Name", chamCong.KHChamCong_ID);
            ViewBag.GiaoVien_ID = new SelectList(db.GiaoViens, "GiaoVien_ID", "GiaoVien_Name", chamCong.GiaoVien_ID);
            return PartialView(chamCong);
        }

        // POST: ChamCongs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ChamCong_ID,NgayChamCong,Lop_ID,GiaoVien_ID,KHChamCong_ID,GiaoVienDayBu_ID,VaiTro")] ChamCong chamCong)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chamCong).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success=true}, JsonRequestBehavior.AllowGet);
            }
            var vaitros = new SelectList(
                new[]
                {
                    new { VaiTro = 0, Name = "Trợ giảng" },
                    new { VaiTro = 1, Name = "Dạy chính" }
                },
                "VaiTro",
                "Name",
                chamCong.KHChamCong_ID
            );
            ViewBag.VaiTro = vaitros;
            ViewBag.Lop_ID = new SelectList(db.Lops, "Lop_ID", "Lop_Name", chamCong.Lop_ID);
            ViewBag.KHChamCong_ID = new SelectList(db.KHChamCongs, "KHChamCong_ID", "KHChamCong_Name", chamCong.KHChamCong_ID);
            ViewBag.GiaoVien_ID = new SelectList(db.GiaoViens, "GiaoVien_ID", "GiaoVien_Name", chamCong.GiaoVien_ID);
            return PartialView(chamCong);
        }

        // GET: ChamCongs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChamCong chamCong = db.ChamCongs.Find(id);
            if (chamCong == null)
            {
                return HttpNotFound();
            }
            return View(chamCong);
        }

        // POST: ChamCongs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChamCong chamCong = db.ChamCongs.Find(id);
            db.ChamCongs.Remove(chamCong);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
