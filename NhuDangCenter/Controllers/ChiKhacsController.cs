﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NhuDangCenter.Models;

namespace NhuDangCenter.Controllers
{
    public class ChiKhacsController : Controller
    {
        private NhuDangCenterEntities db = new NhuDangCenterEntities();

        // GET: ChiKhacs
        public ActionResult Index()
        {
            ViewBag.BangKe = db.ChiKhacs.OrderByDescending(x => x.Ngay).ToList();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include=("TuNgay, DenNgay"))]TuNgayDenNgay obj)
        {
            ViewBag.BangKe = db.ChiKhacs.Where(x => x.Ngay>=obj.TuNgay&& x.Ngay<=obj.DenNgay).OrderByDescending(x => x.Ngay).ToList();
            return View(obj);
        }
        // GET: ChiKhacs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChiKhac chiKhac = db.ChiKhacs.Find(id);
            if (chiKhac == null)
            {
                return HttpNotFound();
            }
            return View(chiKhac);
        }

        // GET: ChiKhacs/Create
        public ActionResult Create()
        {
            return PartialView();
        }

        // POST: ChiKhacs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ChiKhac_ID,SoPhieu,Ngay,SoTien,NoiDung")] ChiKhac chiKhac)
        {
            if (ModelState.IsValid)
            {
                db.ChiKhacs.Add(chiKhac);
                db.SaveChanges();
                return Json(new { success=true}, JsonRequestBehavior.AllowGet);
            }

            return PartialView(chiKhac);
        }

        // GET: ChiKhacs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChiKhac chiKhac = db.ChiKhacs.Find(id);
            if (chiKhac == null)
            {
                return HttpNotFound();
            }
            return PartialView(chiKhac);
        }

        // POST: ChiKhacs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ChiKhac_ID,SoPhieu,Ngay,SoTien,NoiDung")] ChiKhac chiKhac)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chiKhac).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            return PartialView(chiKhac);
        }

        // GET: ChiKhacs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChiKhac chiKhac = db.ChiKhacs.Find(id);
            if (chiKhac == null)
            {
                return HttpNotFound();
            }
            return PartialView(chiKhac);
        }

        // POST: ChiKhacs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChiKhac chiKhac = db.ChiKhacs.Find(id);
            db.ChiKhacs.Remove(chiKhac);
            db.SaveChanges();
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
