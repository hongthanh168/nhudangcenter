﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NhuDangCenter.Models;

namespace NhuDangCenter.Controllers
{
    public class CaController : Controller
    {
        private NhuDangCenterEntities db = new NhuDangCenterEntities();

        // GET: Ca
        public ActionResult Index()
        {
            return View(db.Cas.ToList());
        }

        // GET: Ca/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ca ca = db.Cas.Find(id);
            if (ca == null)
            {
                return HttpNotFound();
            }
            return View(ca);
        }

        // GET: Ca/Create
        public ActionResult Create()
        {
            return PartialView();
        }

        // POST: Ca/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ca_ID,Ca_Name,SoTiet")] Ca ca)
        {
            if (ModelState.IsValid)
            {
                db.Cas.Add(ca);
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }

            return PartialView(ca);
        }

        // GET: Ca/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ca ca = db.Cas.Find(id);
            if (ca == null)
            {
                return HttpNotFound();
            }
            return PartialView(ca);
        }

        // POST: Ca/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ca_ID,Ca_Name,SoTiet")] Ca ca)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ca).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            return PartialView(ca);
        }

        // GET: Ca/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ca ca = db.Cas.Find(id);
            if (ca == null)
            {
                return HttpNotFound();
            }
            return PartialView(ca);
        }

        // POST: Ca/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ca ca = db.Cas.Find(id);
            db.Cas.Remove(ca);
            db.SaveChanges();
            return Json(new { success=true}, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
