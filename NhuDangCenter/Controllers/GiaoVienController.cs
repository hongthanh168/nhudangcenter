﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NhuDangCenter.Models;

namespace NhuDangCenter.Controllers
{
    public class GiaoVienController : Controller
    {
        private NhuDangCenterEntities db = new NhuDangCenterEntities();

        // GET: GiaoVien
        public ActionResult Index()
        {
            var giaoViens = db.GiaoViens.Include(g => g.TrinhDo);
            return View(giaoViens.ToList());
        }

        // GET: GiaoVien/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GiaoVien giaoVien = db.GiaoViens.Find(id);
            if (giaoVien == null)
            {
                return HttpNotFound();
            }
            return View(giaoVien);
        }

        // GET: GiaoVien/Create
        public ActionResult Create()
        {
            ViewBag.TrinhDo_ID = new SelectList(db.TrinhDoes, "TrinhDo_ID", "TrinhDo_Name");
            return PartialView();
        }

        // POST: GiaoVien/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "GiaoVien_ID,GiaoVien_Name,GioiTinh,NgaySinh,DiaChi,DienThoai,Email,TrinhDo_ID,IsGiaoVien,LuongChinh,LuongTroGiang")] GiaoVien giaoVien)
        {
            if (ModelState.IsValid)
            {
                db.GiaoViens.Add(giaoVien);
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Index");
            }

            ViewBag.TrinhDo_ID = new SelectList(db.TrinhDoes, "TrinhDo_ID", "TrinhDo_Name", giaoVien.TrinhDo_ID);
            return View(giaoVien);
        }

        // GET: GiaoVien/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GiaoVien giaoVien = db.GiaoViens.Find(id);
            if (giaoVien == null)
            {
                return HttpNotFound();
            }
            ViewBag.TrinhDo_ID = new SelectList(db.TrinhDoes, "TrinhDo_ID", "TrinhDo_Name", giaoVien.TrinhDo_ID);
            return PartialView(giaoVien);
        }

        // POST: GiaoVien/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "GiaoVien_ID,GiaoVien_Name,GioiTinh,NgaySinh,DiaChi,DienThoai,Email,TrinhDo_ID,IsGiaoVien,LuongChinh,LuongTroGiang")] GiaoVien giaoVien)
        {
            if (ModelState.IsValid)
            {
                db.Entry(giaoVien).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            ViewBag.TrinhDo_ID = new SelectList(db.TrinhDoes, "TrinhDo_ID", "TrinhDo_Name", giaoVien.TrinhDo_ID);
            return PartialView(giaoVien);
        }

        // GET: GiaoVien/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GiaoVien giaoVien = db.GiaoViens.Find(id);
            if (giaoVien == null)
            {
                return HttpNotFound();
            }
            return PartialView(giaoVien);
        }

        // POST: GiaoVien/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GiaoVien giaoVien = db.GiaoViens.Find(id);
            db.GiaoViens.Remove(giaoVien);
            db.SaveChanges();
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
