﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NhuDangCenter.Models;

namespace NhuDangCenter.Controllers.Thanh
{
    public class ThuController : Controller
    {
        private NhuDangCenterEntities db = new NhuDangCenterEntities();

        // GET: Thus
        public ActionResult Index()
        {
            return View(db.Thus.ToList());
        }

        // GET: Thus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Thu thu = db.Thus.Find(id);
            if (thu == null)
            {
                return HttpNotFound();
            }
            return View(thu);
        }

        // GET: Thus/Create
        public ActionResult Create()
        {
            return PartialView();
        }

        // POST: Thus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Thu_ID,Thu_Name,CT_Thu")] Thu thu)
        {
            if (ModelState.IsValid)
            {
                db.Thus.Add(thu);
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }

            return PartialView(thu);
        }

        // GET: Thus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Thu thu = db.Thus.Find(id);
            if (thu == null)
            {
                return HttpNotFound();
            }
            return PartialView(thu);
        }

        // POST: Thus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Thu_ID,Thu_Name,CT_Thu")] Thu thu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thu).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            return PartialView(thu);
        }

        // GET: Thus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Thu thu = db.Thus.Find(id);
            if (thu == null)
            {
                return HttpNotFound();
            }
            return PartialView(thu);
        }

        // POST: Thus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Thu thu = db.Thus.Find(id);
            db.Thus.Remove(thu);
            db.SaveChanges();
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
