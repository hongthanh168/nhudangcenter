﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NhuDangCenter.Models;

namespace NhuDangCenter.Controllers
{
    public class DiemHocVienController : Controller
    {
        private NhuDangCenterEntities db = new NhuDangCenterEntities();

        // GET: DiemHocVien
        [Authorize(Roles = "Giáo viên")]
        public ActionResult Index()
        {
            var cTHocViens = db.CTHocViens.Include(c => c.HocVien).Include(c => c.Lop);
            return View(cTHocViens.ToList());
        }
        //Set cache để nó load lại cái mới cập nhật
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult DiemHocVien(int id)
        {
            var cTHocViens = db.CTHocViens.Where(c=>c.HocVien_ID==id).Include(c => c.Lop);
            ViewBag.CTHocVien_ID = id;
            return PartialView("_DiemHocVien", cTHocViens.ToList());
        }
        // GET: DiemHocVien/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CTHocVien cTHocVien = db.CTHocViens.Find(id);
            if (cTHocVien == null)
            {
                return HttpNotFound();
            }
            return View(cTHocVien);
        }

        // GET: DiemHocVien/Create
        public ActionResult Create()
        {
            ViewBag.HocVien_ID = new SelectList(db.HocViens, "HocVien_ID", "HocVien_Name");
            ViewBag.Lop_ID = new SelectList(db.Lops, "Lop_ID", "MaLop");
            return View();
        }

        // POST: DiemHocVien/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CTHocVien_ID,Lop_ID,HocVien_ID,Diem1,Diem2,Diem3,Diem4")] CTHocVien cTHocVien)
        {
            if (ModelState.IsValid)
            {
                db.CTHocViens.Add(cTHocVien);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.HocVien_ID = new SelectList(db.HocViens, "HocVien_ID", "HocVien_Name", cTHocVien.HocVien_ID);
            ViewBag.Lop_ID = new SelectList(db.Lops, "Lop_ID", "MaLop", cTHocVien.Lop_ID);
            return View(cTHocVien);
        }

        // GET: DiemHocVien/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CTHocVien cTHocVien = db.CTHocViens.Find(id);
            if (cTHocVien == null)
            {
                return HttpNotFound();
            }
            ViewBag.HocVien_ID = new SelectList(db.HocViens, "HocVien_ID", "HocVien_Name", cTHocVien.HocVien_ID);
            ViewBag.Lop_ID = new SelectList(db.Lops, "Lop_ID", "MaLop", cTHocVien.Lop_ID);
            ViewBag.TenHocVien = cTHocVien.HocVien.HocVien_Name;
            ViewBag.TenLop = cTHocVien.Lop.Lop_Name ;
            return PartialView(cTHocVien);
        }

        // POST: DiemHocVien/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CTHocVien_ID,Lop_ID,HocVien_ID,Diem1,Diem2,Diem3,Diem4")] CTHocVien cTHocVien)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cTHocVien).State = EntityState.Modified;
                db.SaveChanges();
                //return RedirectToAction("Index");
                return RedirectToAction("DiemHocVien", new { id = cTHocVien.CTHocVien_ID});
            }
            ViewBag.HocVien_ID = new SelectList(db.HocViens, "HocVien_ID", "HocVien_Name", cTHocVien.HocVien_ID);
            ViewBag.Lop_ID = new SelectList(db.Lops, "Lop_ID", "Lop_Name", cTHocVien.Lop_ID);
            return PartialView(cTHocVien);
        }
        // GET: DiemHocVien/Edit/5
        public ActionResult EditOne(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CTHocVien cTHocVien = db.CTHocViens.Find(id);
            if (cTHocVien == null)
            {
                return HttpNotFound();
            }
            ViewBag.HocVien_ID = new SelectList(db.HocViens, "HocVien_ID", "HocVien_Name", cTHocVien.HocVien_ID);
            ViewBag.Lop_ID = new SelectList(db.Lops, "Lop_ID", "MaLop", cTHocVien.Lop_ID);
            ViewBag.TenHocVien = cTHocVien.HocVien.HocVien_Name;
            ViewBag.TenLop = cTHocVien.Lop.Lop_Name;
            return PartialView(cTHocVien);
        }

        // POST: DiemHocVien/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditOne([Bind(Include = "CTHocVien_ID,Lop_ID,HocVien_ID,Diem1,Diem2,Diem3,Diem4")] CTHocVien cTHocVien)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cTHocVien).State = EntityState.Modified;
                db.SaveChanges();
                //return RedirectToAction("Index");
                //return PartialView(cTHocVien);
                return RedirectToAction("DsDiemHocVien","Lop", new { lopID = cTHocVien.Lop_ID  });
            }
            ViewBag.HocVien_ID = new SelectList(db.HocViens, "HocVien_ID", "HocVien_Name", cTHocVien.HocVien_ID);
            ViewBag.Lop_ID = new SelectList(db.Lops, "Lop_ID", "Lop_Name", cTHocVien.Lop_ID);
            return PartialView(cTHocVien);
        }

        // GET: DiemHocVien/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CTHocVien cTHocVien = db.CTHocViens.Find(id);
            if (cTHocVien == null)
            {
                return HttpNotFound();
            }
            return View(cTHocVien);
        }

        // POST: DiemHocVien/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CTHocVien cTHocVien = db.CTHocViens.Find(id);
            db.CTHocViens.Remove(cTHocVien);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
