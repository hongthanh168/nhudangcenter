﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NhuDangCenter.Models;
using System.Web.Services;
using System.Collections;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;

namespace NhuDangCenter.Controllers
{
   
    public class HocPhiController : Controller
    {
        private NhuDangCenterEntities db = new NhuDangCenterEntities();

        // GET: HocPhi
        public ActionResult Index()
        {
          
            var cTHocPhis = db.CTHocPhis.Include(c => c.CTHocVien);
            ViewBag.HocPhi = cTHocPhis.ToList();
            return View();
        }
        // GET: HocPhi
        public ActionResult ThuHocPhi()
        {

            
            ViewBag.HocPhi = db.store_ThuHocPhi();
            return View();
        }
        //hàm nhập quyết định theo danh sách chọn
        [WebMethod]
        public ActionResult UpdateHocPhiTheoDanhSach(string[] function_param)
        {
            //CẬP NHẬT THÔNG TIN HỌC PHÍ ĐÚNG NHƯ SỐ TIỀN CỦA LỚP
            //Số thứ tự lấy tăng dần

           
            foreach (string item in function_param)
            {
                //Với mỗi cthocvien
                CTHocPhi cTHocPhi = new CTHocPhi();
                
                int macthv = int.Parse(item);
                cTHocPhi.CTHocVien_ID = macthv;
                //Lấy thông tin số tiền học phí theo lớp

                int lopid = Convert.ToInt32(db.CTHocViens.Where(c => c.CTHocVien_ID == macthv).Select(c => c.Lop_ID).First());
                // lấy học phí
                int hocphi = Convert.ToInt32(db.Lops.Where(c => c.Lop_ID == lopid).Select(c => c.HocPhi).First());
                cTHocPhi.SoTien = hocphi;
                cTHocPhi.MienGiam = 0;
                cTHocPhi.ConLai = hocphi;
                cTHocPhi.NgayDongTien = DateTime.Now;
                cTHocPhi.NoiDung = "Đóng học phí";
                //Lấy số thứ tự
                try
                {
                    int stt = db.CTHocPhis.Where(c => c.CTHocVien_ID == macthv).OrderByDescending(c => c.STT).Select(c => c.STT).First();
                    cTHocPhi.STT = stt + 1;
                }
                catch (Exception)
                {

                    cTHocPhi.STT = 1;
                }
                //Chèn vào bảng học phí

                db.CTHocPhis.Add(cTHocPhi);
                db.SaveChanges();
            }
           

            Session["list"] = function_param;
            Session["index"] = 0;
            //char[] delimiterChars = { '|' };
            //string[] list = function_param[0].Split(delimiterChars);
            return new JsonResult { Data = new { e = 0, p = 0, d = 0 } };
            //foreach (var item in function_param)
            //{
            //    //với mỗi người hiển thị nhập bằng cấp
            //    char[] delimiterChars = { '|' };
            //    string[] list = item.Split(delimiterChars);

            //    return View("~/Views/hRM_EMPLOYMENTHISTORY/CreateOne?EmployeeID=341&PlanID=2&detailPlanID=19");
            //    //RedirectToAction("CreateOne", "hRM_EMPLOYMENTHISTORY", new { EmployeeID = list[0], PlanID = list[1], detailPlanID = list[2] });
            //}
            //return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public FileResult Index([Bind(Include = "TuNgay,DenNgay")] TuNgayDenNgay obj)
        {
            var cTHocPhis = db.CTHocPhis.Include(c => c.CTHocVien);
            ViewBag.HocPhi = cTHocPhis.ToList();
            DateTime tuthang = obj.TuNgay;
            DateTime denthang = obj.DenNgay;

            //Xuất file excel

            FileResult result = null;
            string server = Server.MapPath("~/App_Data");

            DirectoryInfo outputDir = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments));

            FileInfo templateFile = new FileInfo(server + "//donghocphi.xltx");
            FileInfo newFile = new FileInfo(outputDir.FullName + @"\dsdonghocphi.xlsx");
            if (newFile.Exists)
            {
                newFile.Delete();  // ensures we create a new workbook
                newFile = new FileInfo(outputDir.FullName + @"\dsdonghocphi.xlsx");
            }
            using (ExcelPackage package = new ExcelPackage(newFile, templateFile))
            {
                //Sheet xuất dữ liệu
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];

                //****************************************
                //CODE INSERT PHIẾU THU
                //****************************************
                //Vị trí row sẽ insert:
                int indexRow = 4;
                //Lấy thông tin học phí
                //LẤY Danh sách
                var hocphi = from a in db.CTHocPhis
                             join b in db.CTHocViens on a.CTHocVien_ID equals b.CTHocVien_ID
                             join c in db.Lops on b.Lop_ID equals c.Lop_ID
                             join d in db.HocViens on b.HocVien_ID equals d.HocVien_ID
                             where a.NgayDongTien >= tuthang && a.NgayDongTien <= denthang
                             select new { d.HocVien_Name, c.Lop_Name, a.NgayDongTien, a.ConLai, a.NoiDung };

                worksheet.Cells[indexRow, 2].Value = "Từ ngày " + tuthang.ToShortDateString();
                worksheet.Cells[indexRow, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                worksheet.Cells[indexRow, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[indexRow, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                worksheet.Cells[indexRow, 3].Value = "đến ngày " + denthang.ToShortDateString();
                worksheet.Cells[indexRow, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                worksheet.Cells[indexRow, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[indexRow, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                //Cập nhật font
                using (ExcelRange r = worksheet.Cells[indexRow, 2, indexRow, 3])
                {
                    r.Style.Font.SetFromFont(new Font("Times New Roman", 13, FontStyle.Italic));
                    //r.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                }
                indexRow = 6;
                int stt = 1; int ins = 0;
                foreach (var item in hocphi)
                {

                    worksheet.Cells[indexRow + ins, 1].Value = stt;
                    worksheet.Cells[indexRow + ins, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    worksheet.Cells[indexRow + ins, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[indexRow + ins, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    worksheet.Cells[indexRow + ins, 2].Value = item.HocVien_Name;
                    worksheet.Cells[indexRow + ins, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    worksheet.Cells[indexRow + ins, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[indexRow + ins, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    worksheet.Cells[indexRow + ins, 3].Value = item.Lop_Name;
                    worksheet.Cells[indexRow + ins, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    worksheet.Cells[indexRow + ins, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[indexRow + ins, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    worksheet.Cells[indexRow + ins, 4].Value = item.ConLai.Value.ToString("#,##0");
                    worksheet.Cells[indexRow + ins, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    worksheet.Cells[indexRow + ins, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[indexRow + ins, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    stt++;
                    ins++;



                    //Cập nhật font
                    using (ExcelRange r = worksheet.Cells[indexRow, 1, indexRow + hocphi.Count() - 1, 4])
                    {
                        r.Style.Font.SetFromFont(new Font("Times New Roman", 13, FontStyle.Regular));
                        r.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    }

                }
                //Switch the PageLayoutView back to normal
                worksheet.View.PageLayoutView = false;
                // save our new workbook and we are done!
                package.Save();


                string absolutePath = newFile.FullName;// = _environment.WebRootPath + @"\" + relativePath;

                if (System.IO.File.Exists(absolutePath))
                {
                    string fileName = System.IO.Path.GetFileName(absolutePath);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(absolutePath);
                    result = File(fileBytes, "application/x-msdownload", fileName);
                }
                return result;
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public FileResult ThuHocPhi([Bind(Include = "TuNgay,DenNgay")] TuNgayDenNgay obj)
        {
            var cTHocPhis = db.CTHocPhis.Include(c => c.CTHocVien);
            ViewBag.HocPhi = cTHocPhis.ToList();
            DateTime tuthang = obj.TuNgay;
            DateTime denthang = obj.DenNgay;

            //Xuất file excel

            FileResult result = null;
            string server = Server.MapPath("~/App_Data");

            DirectoryInfo outputDir = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments));

            FileInfo templateFile = new FileInfo(server + "//donghocphi.xltx");
            FileInfo newFile = new FileInfo(outputDir.FullName + @"\dsdonghocphi.xlsx");
            if (newFile.Exists)
            {
                newFile.Delete();  // ensures we create a new workbook
                newFile = new FileInfo(outputDir.FullName + @"\dsdonghocphi.xlsx");
            }
            using (ExcelPackage package = new ExcelPackage(newFile, templateFile))
            {
                //Sheet xuất dữ liệu
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];

                //****************************************
                //CODE INSERT PHIẾU THU
                //****************************************
                //Vị trí row sẽ insert:
                int indexRow = 4;
                //Lấy thông tin học phí
                //LẤY Danh sách
                var hocphi = from a in db.CTHocPhis
                             join b in db.CTHocViens on a.CTHocVien_ID equals b.CTHocVien_ID
                             join c in db.Lops on b.Lop_ID equals c.Lop_ID
                             join d in db.HocViens on b.HocVien_ID equals d.HocVien_ID
                             where a.NgayDongTien >= tuthang && a.NgayDongTien <= denthang
                             select new { d.HocVien_Name, c.Lop_Name, a.NgayDongTien, a.ConLai, a.NoiDung };

                worksheet.Cells[indexRow, 2].Value = "Từ ngày " + tuthang.ToShortDateString();
                worksheet.Cells[indexRow, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                worksheet.Cells[indexRow, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[indexRow, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                worksheet.Cells[indexRow, 3].Value = "đến ngày " + denthang.ToShortDateString();
                worksheet.Cells[indexRow, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                worksheet.Cells[indexRow, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[indexRow, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                //Cập nhật font
                using (ExcelRange r = worksheet.Cells[indexRow, 2, indexRow, 3])
                {
                    r.Style.Font.SetFromFont(new Font("Times New Roman", 13, FontStyle.Italic));
                    //r.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                }
                indexRow = 6;
                int stt = 1; int ins = 0;
                foreach (var item in hocphi)
                {

                    worksheet.Cells[indexRow + ins, 1].Value = stt;
                    worksheet.Cells[indexRow + ins, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    worksheet.Cells[indexRow + ins, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[indexRow + ins, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    worksheet.Cells[indexRow + ins, 2].Value = item.HocVien_Name;
                    worksheet.Cells[indexRow + ins, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    worksheet.Cells[indexRow + ins, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[indexRow + ins, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    worksheet.Cells[indexRow + ins, 3].Value = item.Lop_Name;
                    worksheet.Cells[indexRow + ins, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    worksheet.Cells[indexRow + ins, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[indexRow + ins, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    worksheet.Cells[indexRow + ins, 4].Value = item.ConLai.Value.ToString("#,##0");
                    worksheet.Cells[indexRow + ins, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    worksheet.Cells[indexRow + ins, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[indexRow + ins, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    stt++;
                    ins++;



                    //Cập nhật font
                    using (ExcelRange r = worksheet.Cells[indexRow, 1, indexRow + hocphi.Count() - 1, 4])
                    {
                        r.Style.Font.SetFromFont(new Font("Times New Roman", 13, FontStyle.Regular));
                        r.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    }

                }
                //Switch the PageLayoutView back to normal
                worksheet.View.PageLayoutView = false;
                // save our new workbook and we are done!
                package.Save();


                string absolutePath = newFile.FullName;// = _environment.WebRootPath + @"\" + relativePath;

                if (System.IO.File.Exists(absolutePath))
                {
                    string fileName = System.IO.Path.GetFileName(absolutePath);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(absolutePath);
                    result = File(fileBytes, "application/x-msdownload", fileName);
                }
                return result;
            }
        }
        // GET: HocPhi
        public ActionResult DanhSachNoHocPhi()
        {

            var cTHocPhis = db.store_NoHocPhi().Where(c=>c.ConNo!=0);
            return View(cTHocPhis.ToList());
        }
                  
       
        public FileResult XuatDsNoHocPhi()
        {
       
            //Xuất file excel

            FileResult result = null;
            string server = Server.MapPath("~/App_Data");

            DirectoryInfo outputDir = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments));

            FileInfo templateFile = new FileInfo(server + "//nohocphi.xltx");
            FileInfo newFile = new FileInfo(outputDir.FullName + @"\dsnohocphi.xlsx");
            if (newFile.Exists)
            {
                newFile.Delete();  // ensures we create a new workbook
                newFile = new FileInfo(outputDir.FullName + @"\dsnohocphi.xlsx");
            }
            using (ExcelPackage package = new ExcelPackage(newFile, templateFile))
            {
                //Sheet xuất dữ liệu
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];

                //****************************************
                //CODE INSERT PHIẾU THU
                //****************************************
                //Vị trí row sẽ insert:
                int indexRow = 6;
                //Lấy thông tin học phí
                //LẤY Danh sách
                var hocphi = db.store_NoHocPhi().Where(c => c.ConNo != 0);

              
                int stt = 1;int ins = 0;
                foreach (var item in hocphi)
                {

                    worksheet.Cells[indexRow+ins, 1].Value = stt;
                    worksheet.Cells[indexRow + ins,1].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    worksheet.Cells[indexRow + ins,1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[indexRow + ins,1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    worksheet.Cells[indexRow + ins, 2].Value = item.HocVien_Name;
                    worksheet.Cells[indexRow + ins, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    worksheet.Cells[indexRow + ins, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[indexRow + ins, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    worksheet.Cells[indexRow + ins, 3].Value = item.Lop_Name;
                    worksheet.Cells[indexRow + ins, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    worksheet.Cells[indexRow + ins, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[indexRow + ins, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    
                    worksheet.Cells[indexRow + ins, 4].Value = item.HocPhi.ToString("#,##0");
                    worksheet.Cells[indexRow + ins, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    worksheet.Cells[indexRow + ins, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[indexRow + ins, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    worksheet.Cells[indexRow + ins, 5].Value = item.DaNop.ToString("#,##0");
                    worksheet.Cells[indexRow + ins, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    worksheet.Cells[indexRow + ins, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[indexRow + ins, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    worksheet.Cells[indexRow + ins, 6].Value = item.ConNo.ToString("#,##0");
                    worksheet.Cells[indexRow + ins, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    worksheet.Cells[indexRow + ins, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[indexRow + ins, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    stt++;
                    ins++;



               
                }
                ////Cập nhật font
                //using (ExcelRange r = worksheet.Cells[indexRow, 1, indexRow + hocphi.Count() - 1, 6])
                //{
                //    r.Style.Font.SetFromFont(new Font("Times New Roman", 13, FontStyle.Regular));
                //    r.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                //}

                //Switch the PageLayoutView back to normal
                worksheet.View.PageLayoutView = false;
                // save our new workbook and we are done!
                package.Save();


                string absolutePath = newFile.FullName;// = _environment.WebRootPath + @"\" + relativePath;

                if (System.IO.File.Exists(absolutePath))
                {
                    string fileName = System.IO.Path.GetFileName(absolutePath);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(absolutePath);
                    result = File(fileBytes, "application/x-msdownload", fileName);
                }
                return result;
            }
             
        }

        // GET: HocPhi/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CTHocPhi cTHocPhi = db.CTHocPhis.Find(id);
            if (cTHocPhi == null)
            {
                return HttpNotFound();
            }
            return View(cTHocPhi);
        }

        // GET: HocPhi/Create
        public ActionResult Create()
        {
            var dsHocVien = (from a in db.CTHocViens
                             join b in db.HocViens on a.HocVien_ID equals b.HocVien_ID
                             select new { HocVien_ID = b.HocVien_ID, HocVien_Name = b.HocVien_Name}).Distinct().ToList();
            var dsLop = (from a in db.CTHocViens
                             join b in db.Lops on a.Lop_ID equals b.Lop_ID
                         select new {Lop_ID = b.Lop_ID, Lop_Name = b.Lop_Name }).Distinct().ToList();
            ViewBag.CTHocVien_ID = new SelectList(db.CTHocViens, "CTHocVien_ID", "CTHocVien_ID");
            ViewBag.HocVien = new SelectList(dsHocVien, "HocVien_ID", "HocVien_Name");
            ViewBag.Lop = new SelectList(dsLop, "Lop_ID", "Lop_Name");
            return PartialView();
        }

        // POST: HocPhi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CTHocVien_ID,STT,NgayDongTien,SoTien,MienGiam,ConLai,NoiDung,LyDoMienGiam")] CTHocPhi cTHocPhi, FormCollection collection)
        {
            int hocvien_id =Int32.Parse( collection["HocVien_ID"]);
            int lop_id = Int32.Parse(collection["Lop_ID"]);
            //Kiểm tra lấy cthocvienid
            int cthocvien_id = db.CTHocViens.Where(c => c.Lop_ID == lop_id & c.HocVien_ID == hocvien_id).Select(c => c.CTHocVien_ID).First();
            cTHocPhi.CTHocVien_ID = cthocvien_id;
            //Số thứ tự lấy tăng dần
            try
            {
                int stt = db.CTHocPhis.Where(c => c.CTHocVien_ID == cthocvien_id).OrderByDescending(c => c.STT).Select(c => c.STT).First();
                cTHocPhi.STT = stt + 1;
            }
            catch (Exception)
            {

                cTHocPhi.STT = 1;
            }
            
            if (ModelState.IsValid)
            {
                db.CTHocPhis.Add(cTHocPhi);
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Index");
            }

            ViewBag.CTHocVien_ID = new SelectList(db.CTHocViens, "CTHocVien_ID", "CTHocVien_ID", cTHocPhi.CTHocVien_ID);
            return View(cTHocPhi);
        }
        //HAM GIAM TIỀN
        [WebMethod]
        public JsonResult MienGiam(int SoTien, int MienGiam)
        {
            ArrayList arl = new ArrayList();


            int TienNop = SoTien * (100 - MienGiam) /100;

             

                arl.Add(new { MienGiam =TienNop });

 

            return new JsonResult { Data = arl };
        }

        [WebMethod]
        public JsonResult GetLop(int HocVien_ID)
        {
            ArrayList arl = new ArrayList();


            var dsLop = (from a in db.CTHocViens
                         join b in db.Lops on a.Lop_ID equals b.Lop_ID
                         where a.HocVien_ID == HocVien_ID
                         select b).ToList();

            foreach (Lop rg in dsLop)
            {
                arl.Add(new { Lop_ID = rg.Lop_ID, Lop_Name=rg.Lop_Name });
            }
            return Json(arl, JsonRequestBehavior.AllowGet);
            //return new JsonResult { Data = arl };
        }
        // GET: HocPhi/Edit/5
        public ActionResult Edit(int? id, int stt)
        {
            

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           
            CTHocPhi cTHocPhi = db.CTHocPhis.Find(id,stt);
            if (cTHocPhi == null)
            {
                return HttpNotFound();
            }
            string dsHocVien = (from a in db.CTHocViens
                             join b in db.HocViens on a.HocVien_ID equals b.HocVien_ID
                             where a.CTHocVien_ID == id
                             select b.HocVien_Name).First();
            string dsLop = (from a in db.CTHocViens
                            join b in db.Lops on a.Lop_ID equals b.Lop_ID
                            where a.CTHocVien_ID == id
                            select b.Lop_Name).First();

            //ViewBag.CTHocVien_ID = new SelectList(db.CTHocViens, "CTHocVien_ID", "CTHocVien_ID");
            ViewBag.HocVien = dsHocVien;  // new SelectList(dsHocVien, "HocVien_ID", "HocVien_Name");
            ViewBag.Lop = dsLop;// new SelectList(dsLop, "Lop_ID", "Lop_Name");
            //ViewBag.CTHocVien_ID = new SelectList(db.CTHocViens, "CTHocVien_ID", "CTHocVien_ID", cTHocPhi.CTHocVien_ID);
            return PartialView(cTHocPhi);
        }

        // POST: HocPhi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CTHocVien_ID,STT,NgayDongTien,SoTien,MienGiam,ConLai,NoiDung,LyDoMienGiam")] CTHocPhi cTHocPhi)
        {
                      
                         
             
            if (ModelState.IsValid)
            {
                db.Entry(cTHocPhi).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            //ViewBag.CTHocVien_ID = new SelectList(db.CTHocViens, "CTHocVien_ID", "CTHocVien_ID", cTHocPhi.CTHocVien_ID);
            return View(cTHocPhi);
        }

        // GET: HocPhi/Delete/5
        public ActionResult Delete(int? id, int stt)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CTHocPhi cTHocPhi = db.CTHocPhis.Where(c=>c.CTHocVien_ID==id & c.STT ==stt).First();
            if (cTHocPhi == null)
            {
                return HttpNotFound();
            }
            return PartialView(cTHocPhi);
        }

        // POST: HocPhi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int stt)
        {
            CTHocPhi cTHocPhi = db.CTHocPhis.Where(c => c.CTHocVien_ID == id & c.STT == stt).First();
            db.CTHocPhis.Remove(cTHocPhi);
            db.SaveChanges();
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
      
         public FileResult PhieuThu(int cthv, int stt1)
        {

            FileResult result = null;
            string server = Server.MapPath("~/App_Data");

            DirectoryInfo outputDir = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments));

            FileInfo templateFile = new FileInfo(server + "//phieuthu.xltx");
            FileInfo newFile = new FileInfo(outputDir.FullName + @"\phieuthu.xlsx");
            if (newFile.Exists)
            {
                newFile.Delete();  // ensures we create a new workbook
                newFile = new FileInfo(outputDir.FullName + @"\phieuthu.xlsx");
            }
            using (ExcelPackage package = new ExcelPackage(newFile, templateFile))
            {
                //Sheet xuất dữ liệu
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];

                //****************************************
                //CODE INSERT PHIẾU THU
                //****************************************
                //Vị trí row sẽ insert:
                int indexRow = 4;
                //Lấy thông tin học phí
                //LẤY THÔNG TIN CHI TIẾT ĐỂ IN PHIẾU THU
                var hocphi = (from a in db.CTHocPhis
                              join b in db.CTHocViens on a.CTHocVien_ID equals b.CTHocVien_ID
                              join c in db.Lops on b.Lop_ID equals c.Lop_ID
                              join d in db.HocViens on b.HocVien_ID equals d.HocVien_ID
                              where a.STT == stt1 && a.CTHocVien_ID == cthv
                              select new { d.HocVien_Name, c.Lop_Name, a.NgayDongTien, a.ConLai, a.NoiDung }).First();

                worksheet.Cells[indexRow, 4].Value = "Ngày " + Convert.ToDateTime(hocphi.NgayDongTien).Day + " tháng " + Convert.ToDateTime(hocphi.NgayDongTien).Month + " năm " + Convert.ToDateTime(hocphi.NgayDongTien).Year;
                //worksheet.Cells[indexRow, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                worksheet.Cells[indexRow, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[indexRow, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                //Cập nhật font
                using (ExcelRange r = worksheet.Cells[indexRow, 4, indexRow , 4])
                {
                    r.Style.Font.SetFromFont(new Font("Times New Roman", 13, FontStyle.Italic));
                   
                }
                //họ tên
                worksheet.Cells[indexRow + 1, 3].Value = hocphi.HocVien_Name;
                //worksheet.Cells[indexRow + 1, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                worksheet.Cells[indexRow + 1, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[indexRow + 1, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                //Địa chỉ
                worksheet.Cells[indexRow + 2, 3].Value = hocphi.Lop_Name;
                //worksheet.Cells[indexRow + 2, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                worksheet.Cells[indexRow + 2, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[indexRow + 2, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                //Lý do nộp
                worksheet.Cells[indexRow + 3, 3].Value = hocphi.NoiDung;
                //worksheet.Cells[indexRow + 3, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                worksheet.Cells[indexRow + 3, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[indexRow + 3, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                //Số tiền

                worksheet.Cells[indexRow + 4, 3].Value = hocphi.ConLai.Value.ToString("#,##0");
                //worksheet.Cells[indexRow + 4, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                worksheet.Cells[indexRow + 4, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[indexRow + 4, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                //worksheet.Cells[indexRow + row, 4].Style.Numberformat.Format = "#,##0";



                ////Cập nhật font
                //using (ExcelRange r = worksheet.Cells[indexRow, 1, indexRow + insertedRows - 1, 6])
                //{
                //    r.Style.Font.SetFromFont(new Font("Times New Roman", 13, FontStyle.Regular));
                //    r.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                //}


                //Switch the PageLayoutView back to normal
                worksheet.View.PageLayoutView = false;
                // save our new workbook and we are done!
                package.Save();


                string absolutePath = newFile.FullName;// = _environment.WebRootPath + @"\" + relativePath;

                if (System.IO.File.Exists(absolutePath))
                {
                    string fileName = System.IO.Path.GetFileName(absolutePath);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(absolutePath);
                    result = File(fileBytes, "application/x-msdownload", fileName);
                }
                return result;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
