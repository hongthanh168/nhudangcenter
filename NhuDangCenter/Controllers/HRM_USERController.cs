﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NhuDangCenter.Models;

namespace NhuDangCenter.Controllers
{
    public class HRM_USERController : Controller
    {
        private NhuDangCenterEntities db = new NhuDangCenterEntities();

        public JsonResult IsExists(string UserName)
        {
            //check if any of the UserName matches the UserName specified in the Parameter using the ANY extension method.  
            return Json(!db.HRM_USER.Any(x => x.UserName == UserName), JsonRequestBehavior.AllowGet);
        }

        // GET: HRM_USER
        public ActionResult Index()
        {
            return View(db.HRM_USER.Include(u=>u.HRM_USERROLES).ToList());
        }

        // GET: HRM_USER/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HRM_USER hRM_USER = db.HRM_USER.Find(id);
            if (hRM_USER == null)
            {
                return HttpNotFound();
            }
            return View(hRM_USER);
        }

        // GET: HRM_USER/Create
        public ActionResult Create()
        {
            return PartialView();
        }

        // POST: HRM_USER/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserID,UserName,Password,FullName")] HRM_USER hRM_USER)
        {
            if (ModelState.IsValid)
            {
                hRM_USER.Password = ThanhUtilities.GetMd5Hash(hRM_USER.Password);
                db.HRM_USER.Add(hRM_USER);
                db.SaveChanges();
                return Json(new { success=true}, JsonRequestBehavior.AllowGet);
            }

            return PartialView(hRM_USER);
        }

        // GET: HRM_USER/Edit/5
        public ActionResult EditUserRoles(int id)
        {
            var us = db.HRM_USER.Find(id);
            if (us == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = us.UserID;
            ViewBag.TenTaiKhoan = us.FullName;
            var roles = (from r in db.HRM_ROLE
                                where !db.HRM_USERROLES.Any(f => f.UserID == id && f.RoleID == r.RoleID)
                                select r);
            return PartialView(roles.ToList());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUserRoles(int UserID, int[] chkRole)
        {
            if (ModelState.IsValid)
            {
                foreach (int id in chkRole)
                {
                    HRM_USERROLES ur = new HRM_USERROLES();
                    ur.UserID = UserID;
                    ur.RoleID = id;
                    db.HRM_USERROLES.Add(ur);
                }
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }

            var us = db.HRM_USER.Find(UserID);
            if (us == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = us.UserID;
            ViewBag.TenTaiKhoan = us.FullName;
            var roles = (from r in db.HRM_ROLE
                         where !db.HRM_USERROLES.Any(f => f.UserID == UserID && f.RoleID == r.RoleID)
                         select r);
            return PartialView(roles.ToList());

        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HRM_USER hRM_USER = db.HRM_USER.Find(id);
            if (hRM_USER == null)
            {
                return HttpNotFound();
            }
            return View(hRM_USER);
        }

        // POST: HRM_USER/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserID,UserName,Password,FullName")] HRM_USER hRM_USER)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hRM_USER).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(hRM_USER);
        }
        public ActionResult ChangePassword(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HRM_USER hRM_USER = db.HRM_USER.Find(id);
            if (hRM_USER == null)
            {
                return HttpNotFound();
            }
            return PartialView(hRM_USER);
        }

        // POST: HRM_USER/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(string Password, int UserID)
        {
            HRM_USER obj = db.HRM_USER.Find(UserID);
            if (ModelState.IsValid)
            {
                obj.Password = ThanhUtilities.GetMd5Hash(Password);
                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success=true}, JsonRequestBehavior.AllowGet);
            }
            return PartialView(obj);
        }
        // GET: HRM_USER/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HRM_USER hRM_USER = db.HRM_USER.Find(id);
            if (hRM_USER == null)
            {
                return HttpNotFound();
            }
            return PartialView(hRM_USER);
        }

        // POST: HRM_USER/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HRM_USER hRM_USER = db.HRM_USER.Find(id);
            db.HRM_USER.Remove(hRM_USER);
            db.SaveChanges();
            return Json(new { success=true},JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
