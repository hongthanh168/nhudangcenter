﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NhuDangCenter.Models;

namespace NhuDangCenter.Controllers.Thanh
{
    public class CapDoController : Controller
    {
        private NhuDangCenterEntities db = new NhuDangCenterEntities();

        // GET: CapDo
        public ActionResult Index()
        {
            var capDoes = db.CapDoes.Include(c => c.ChuongTrinh);
            return View(capDoes.ToList());
        }

        // GET: CapDo/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CapDo capDo = db.CapDoes.Find(id);
            if (capDo == null)
            {
                return HttpNotFound();
            }
            return View(capDo);
        }

        // GET: CapDo/Create
        public ActionResult Create()
        {
            ViewBag.ChuongTrinh_ID = new SelectList(db.ChuongTrinhs, "ChuongTrinh_ID", "ChuongTrinh_Name");
            return PartialView();
        }

        // POST: CapDo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CapDo_ID,CapDo_Name,ChuongTrinh_ID")] CapDo capDo)
        {
            if (ModelState.IsValid)
            {
                db.CapDoes.Add(capDo);
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }

            ViewBag.ChuongTrinh_ID = new SelectList(db.ChuongTrinhs, "ChuongTrinh_ID", "ChuongTrinh_Name", capDo.ChuongTrinh_ID);
            return PartialView(capDo);
        }

        // GET: CapDo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CapDo capDo = db.CapDoes.Find(id);
            if (capDo == null)
            {
                return HttpNotFound();
            }
            ViewBag.ChuongTrinh_ID = new SelectList(db.ChuongTrinhs, "ChuongTrinh_ID", "ChuongTrinh_Name", capDo.ChuongTrinh_ID);
            return PartialView(capDo);
        }

        // POST: CapDo/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CapDo_ID,CapDo_Name,ChuongTrinh_ID")] CapDo capDo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(capDo).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            ViewBag.ChuongTrinh_ID = new SelectList(db.ChuongTrinhs, "ChuongTrinh_ID", "ChuongTrinh_Name", capDo.ChuongTrinh_ID);
            return PartialView(capDo);
        }

        // GET: CapDo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CapDo capDo = db.CapDoes.Find(id);
            if (capDo == null)
            {
                return HttpNotFound();
            }
            return PartialView(capDo);
        }

        // POST: CapDo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CapDo capDo = db.CapDoes.Find(id);
            db.CapDoes.Remove(capDo);
            db.SaveChanges();
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
