﻿--Ngày 24/07
-- Sửa dnah sách nợ học phí
USE [NhuDangCenter]
GO
/****** Object:  StoredProcedure [dbo].[store_NoHocPhi]    Script Date: 7/24/2017 4:52:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[store_NoHocPhi]
as
begin
SELECT        dbo.HocVien.HocVien_Name, isnull(dbo.Lop.HocPhi,0) AS HocPhi, isnull(SUM( dbo.CTHocPhi.ConLai),0) AS DaNop,
isnull(dbo.Lop.HocPhi - SUM( dbo.CTHocPhi.ConLai),0) AS ConNo, dbo.Lop.Lop_ID, dbo.Lop.Lop_Name
FROM            dbo.Lop INNER JOIN
                         dbo.CTHocVien ON dbo.Lop.Lop_ID = dbo.CTHocVien.Lop_ID INNER JOIN
                         dbo.HocVien ON dbo.CTHocVien.HocVien_ID = dbo.HocVien.HocVien_ID INNER JOIN
                         dbo.CTHocPhi ON dbo.CTHocVien.CTHocVien_ID = dbo.CTHocPhi.CTHocVien_ID
GROUP BY dbo.HocVien.HocVien_Name, dbo.Lop.Lop_ID, dbo.Lop.Lop_Name, lop.hocphi
having dbo.Lop.HocPhi >= SUM( dbo.CTHocPhi.ConLai)
end




-- Ngày 19/6/2017
-- Store danh sách thu học phí
CREATE PROCEDURE [dbo].[store_ThuHocPhi]
as
begin
SELECT        dbo.CTHocVien.CTHocVien_ID, dbo.HocVien.HocVien_Name, isnull(dbo.Lop.HocPhi,0) AS HocPhi, dbo.Lop.Lop_ID, dbo.Lop.Lop_Name
FROM            dbo.Lop INNER JOIN
                         dbo.CTHocVien ON dbo.Lop.Lop_ID = dbo.CTHocVien.Lop_ID INNER JOIN
                         dbo.HocVien ON dbo.CTHocVien.HocVien_ID = dbo.HocVien.HocVien_ID
WHERE dbo.CTHocVien.CTHocVien_ID NOT IN (select    CTHocVien_ID from cthocphi)
GROUP BY dbo.CTHocVien.CTHocVien_ID,dbo.HocVien.HocVien_Name, dbo.Lop.Lop_ID, dbo.Lop.Lop_Name, lop.hocphi
end
-- Ngày 2/5/2017
-- Store tạo mã học viên
USE [NhuDangCenter]
GO
/****** Object:  StoredProcedure [dbo].[store_TaoMaHocVien]    Script Date: 5/3/2017 2:03:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[store_TaoMaHocVien]
					@nam int
					as
					begin
					select [dbo].[fc_T_TaoMaHocVien](@nam)
					end
-- Sửa khóa tăng cho giáo viên
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.GiaoVien
	DROP CONSTRAINT FK_GiaoVien_TrinhDo
GO
ALTER TABLE dbo.TrinhDo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_GiaoVien
	(
	GiaoVien_ID int NOT NULL IDENTITY (1, 1),
	GiaoVien_Name nvarchar(50) NULL,
	GioiTinh bit NULL,
	NgaySinh smalldatetime NULL,
	DiaChi nvarchar(50) NULL,
	DienThoai nvarchar(50) NULL,
	Email nvarchar(50) NULL,
	TrinhDo_ID int NULL,
	IsGiaoVien bit NULL,
	LuongChinh int NULL,
	LuongTroGiang int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_GiaoVien SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_GiaoVien ON
GO
IF EXISTS(SELECT * FROM dbo.GiaoVien)
	 EXEC('INSERT INTO dbo.Tmp_GiaoVien (GiaoVien_ID, GiaoVien_Name, GioiTinh, NgaySinh, DiaChi, DienThoai, Email, TrinhDo_ID, IsGiaoVien, LuongChinh, LuongTroGiang)
		SELECT GiaoVien_ID, GiaoVien_Name, GioiTinh, NgaySinh, DiaChi, DienThoai, Email, TrinhDo_ID, IsGiaoVien, LuongChinh, LuongTroGiang FROM dbo.GiaoVien WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_GiaoVien OFF
GO
ALTER TABLE dbo.Lop
	DROP CONSTRAINT FK_Lop_GiaoVien
GO
ALTER TABLE dbo.Lop
	DROP CONSTRAINT FK_Lop_GiaoVien1
GO
ALTER TABLE dbo.LuongGiaoVien
	DROP CONSTRAINT FK_LuongGiaoVien_GiaoVien
GO
ALTER TABLE dbo.ChamCong
	DROP CONSTRAINT FK_ChamCong_GiaoVien
GO
ALTER TABLE dbo.ChamCong
	DROP CONSTRAINT FK_ChamCong_GiaoVien1
GO
DROP TABLE dbo.GiaoVien
GO
EXECUTE sp_rename N'dbo.Tmp_GiaoVien', N'GiaoVien', 'OBJECT' 
GO
ALTER TABLE dbo.GiaoVien ADD CONSTRAINT
	PK_GiaoVien PRIMARY KEY CLUSTERED 
	(
	GiaoVien_ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.GiaoVien ADD CONSTRAINT
	FK_GiaoVien_TrinhDo FOREIGN KEY
	(
	TrinhDo_ID
	) REFERENCES dbo.TrinhDo
	(
	TrinhDo_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ChamCong ADD CONSTRAINT
	FK_ChamCong_GiaoVien FOREIGN KEY
	(
	GiaoVien_ID
	) REFERENCES dbo.GiaoVien
	(
	GiaoVien_ID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.ChamCong ADD CONSTRAINT
	FK_ChamCong_GiaoVien1 FOREIGN KEY
	(
	GiaoVienDayBu_ID
	) REFERENCES dbo.GiaoVien
	(
	GiaoVien_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ChamCong SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.LuongGiaoVien ADD CONSTRAINT
	FK_LuongGiaoVien_GiaoVien FOREIGN KEY
	(
	GiaoVien_ID
	) REFERENCES dbo.GiaoVien
	(
	GiaoVien_ID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.LuongGiaoVien SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Lop ADD CONSTRAINT
	FK_Lop_GiaoVien FOREIGN KEY
	(
	GiaoVien_ID
	) REFERENCES dbo.GiaoVien
	(
	GiaoVien_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Lop ADD CONSTRAINT
	FK_Lop_GiaoVien1 FOREIGN KEY
	(
	TroGiang_ID
	) REFERENCES dbo.GiaoVien
	(
	GiaoVien_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Lop SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
-- 24/4/2017
USE [NhuDangCenter]
GO
/****** Object:  StoredProcedure [dbo].[store_NoHocPhi]    Script Date: 4/24/2017 9:42:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[store_NoHocPhi]
as
begin
SELECT        dbo.HocVien.HocVien_Name, isnull(dbo.Lop.HocPhi,0) AS HocPhi, isnull(SUM(DISTINCT dbo.CTHocPhi.ConLai),0) AS DaNop,
isnull(SUM(dbo.Lop.HocPhi) - SUM(DISTINCT dbo.CTHocPhi.ConLai),0) AS ConNo, dbo.Lop.Lop_ID, dbo.Lop.Lop_Name
FROM            dbo.Lop INNER JOIN
                         dbo.CTHocVien ON dbo.Lop.Lop_ID = dbo.CTHocVien.Lop_ID INNER JOIN
                         dbo.HocVien ON dbo.CTHocVien.HocVien_ID = dbo.HocVien.HocVien_ID INNER JOIN
                         dbo.CTHocPhi ON dbo.CTHocVien.CTHocVien_ID = dbo.CTHocPhi.CTHocVien_ID
GROUP BY dbo.HocVien.HocVien_Name, dbo.Lop.Lop_ID, dbo.Lop.Lop_Name, lop.hocphi
end

 GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: 24/04/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[store_ThoiKhoaBieu]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT    distinct    dbo.Ca.Ca_Name, dbo.Thu.CT_Thu, dbo.Lop.Lop_Name, dbo.GiaoVien.GiaoVien_Name, dbo.Phong.Phong_Name, 
	dbo.Lop.BatDau, dbo.Lop.KetThuc, dbo.Lop.HocPhi
FROM            dbo.Ca INNER JOIN
                         dbo.Lop ON dbo.Ca.Ca_ID = dbo.Lop.Ca_ID INNER JOIN
                         dbo.GiaoVien ON dbo.Lop.GiaoVien_ID = dbo.GiaoVien.GiaoVien_ID INNER JOIN
                         dbo.Thu ON dbo.Lop.Thu_ID = dbo.Thu.Thu_ID INNER JOIN
                         dbo.Phong ON dbo.Lop.Phong_ID = dbo.Phong.Phong_ID
		WHERE Lop.Ketthuc>= GetDate()
		Order by Ca.Ca_Name
END

------------------------------------------------
USE [NhuDangCenter]
GO
/****** Object:  StoredProcedure [dbo].[store_ThoiKhoaBieu]    Script Date: 4/19/2017 3:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[store_ThoiKhoaBieu]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT        dbo.Ca.Ca_Name, dbo.Thu.CT_Thu, dbo.Lop.Lop_Name, dbo.GiaoVien.GiaoVien_Name, dbo.Phong.Phong_Name, 
	dbo.Lop.BatDau, dbo.Lop.KetThuc, dbo.Lop.HocPhi
FROM            dbo.Ca INNER JOIN
                         dbo.Lop ON dbo.Ca.Ca_ID = dbo.Lop.Ca_ID INNER JOIN
                         dbo.GiaoVien ON dbo.Lop.GiaoVien_ID = dbo.GiaoVien.GiaoVien_ID INNER JOIN
                         dbo.Thu ON dbo.Lop.Thu_ID = dbo.Thu.Thu_ID INNER JOIN
                         dbo.Phong ON dbo.Lop.Phong_ID = dbo.Phong.Phong_ID
		--WHERE Lop.BatDau >= GetDate()
		Order by Ca.Ca_Name
END

-------------------------------------
 
GO
/****** Object:  StoredProcedure [dbo].[store_chuyenHocVien_Lop]    Script Date: 4/19/2017 3:36:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[store_chuyenHocVien_Lop] 
	-- Add the parameters for the stored procedure here
@LopcuID int, @LopmoiID int, @HocVienID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--Xóa học viên ở lớp cũ
	delete from CTHocVien where hocvien_id = @HocVienID and Lop_ID = @LopcuID
	--Thêm học viên vào lớp mới
	--Kiểm tra xem học viên đã có trong lớp mới hay chưa
	declare @row int
	set @row =0
	select @row = hocvien_id from cthocvien where hocvien_id = @HocVienID and Lop_ID = @LopmoiID
	if	@row = 0
   INSERT INTO [dbo].[CTHocVien]
           ([Lop_ID]
           ,[HocVien_ID]
           ,[Diem1]
           ,[Diem2]
           ,[Diem3]
           ,[Diem4])
     VALUES
           (@LopmoiID,@HocVienID,NULL,NULL,NULL,NULL )
END

-------------------------------

 GO
/****** Object:  StoredProcedure [dbo].[store_Lop_HocVien]    Script Date: 4/5/2017 3:49:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[store_Lop_HocVien]
	 @lopid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select  * from hocvien 
	where  hocvien_id not in (select hocvien_id from cthocvien where lop_id = @lopid)

END

GO
USE [NhuDangCenter]
GO
/****** Object:  StoredProcedure [dbo].[store_Lop_HocVien1]    Script Date: 4/5/2017 3:50:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[store_Lop_HocVien1]
	 @lopid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select  * from hocvien 
	where  hocvien_id in (select hocvien_id from cthocvien where lop_id = @lopid)

END
GO
USE [NhuDangCenter]
GO
/****** Object:  StoredProcedure [dbo].[InsertHocVien_Lop]    Script Date: 4/5/2017 3:50:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InsertHocVien_Lop] 
	-- Add the parameters for the stored procedure here
@HocVienID int, @LopID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--Chèn bảng điểm
   INSERT INTO [dbo].[CTHocVien]
           ([Lop_ID]
           ,[HocVien_ID]
           ,[Diem1]
           ,[Diem2]
           ,[Diem3]
           ,[Diem4])
     VALUES
           (@LopID,@HocVienID,NULL,NULL,NULL,NULL )
END

----------------
USE [NhuDangCenter]
GO
/****** Object:  StoredProcedure [dbo].[store_ThoiKhoaBieu]    Script Date: 4/10/2017 10:21:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[store_ThoiKhoaBieu]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT        dbo.Ca.Ca_Name, dbo.Thu.CT_Thu, dbo.Lop.Lop_Name, dbo.GiaoVien.GiaoVien_Name, dbo.Phong.Phong_Name
FROM            dbo.Ca INNER JOIN
                         dbo.Lop ON dbo.Ca.Ca_ID = dbo.Lop.Ca_ID INNER JOIN
                         dbo.GiaoVien ON dbo.Lop.GiaoVien_ID = dbo.GiaoVien.GiaoVien_ID INNER JOIN
                         dbo.Thu ON dbo.Lop.Thu_ID = dbo.Thu.Thu_ID INNER JOIN
                         dbo.Phong ON dbo.Lop.Phong_ID = dbo.Phong.Phong_ID
		--WHERE Lop.BatDau >= GetDate()
		Order by Ca.Ca_Name
END

