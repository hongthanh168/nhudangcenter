﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Word;
using NhuDangCenter.Models;
using OfficeOpenXml;
using OfficeOpenXml.Style;



namespace NhuDangCenter.Controllers
{
    public class CExcel
    {
        private NhuDangCenterEntities dc = new NhuDangCenterEntities();
      
        

        
          
        public void PhieuThu(String server, int cthv, int stt1)
        {



            object xlApp;
            Workbook xlBook;
            Worksheet xlSheet;
            Type t = Type.GetTypeFromProgID("Excel.Application");
            xlApp = System.Activator.CreateInstance(t);

            String link = server + "//phieuthu.xltx";
            xlBook = ((Microsoft.Office.Interop.Excel.Application)xlApp).Workbooks.Open(link);
            xlSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlBook.Worksheets.get_Item(1);
            ((Microsoft.Office.Interop.Excel.Application)xlApp).Visible = true;

            //LẤY THÔNG TIN CHI TIẾT ĐỂ IN PHIẾU THU
            var hocphi = (from a in dc.CTHocPhis
                          join b in dc.CTHocViens on a.CTHocVien_ID equals b.CTHocVien_ID
                          join c in dc.Lops on b.Lop_ID equals c.Lop_ID
                          join d in dc.HocViens on b.HocVien_ID equals d.HocVien_ID
                          where a.STT == stt1 && a.CTHocVien_ID == cthv
                          select new { d.HocVien_Name, c.Lop_Name, a.NgayDongTien, a.ConLai, a.NoiDung }).First();



            Microsoft.Office.Interop.Excel.Range mrange;
            int row = 4;
            //1. Ngày tháng
            xlSheet.Cells[row, 4] = "Ngày " + Convert.ToDateTime(hocphi.NgayDongTien).Day + " tháng " + Convert.ToDateTime(hocphi.NgayDongTien).Month + " năm " + Convert.ToDateTime(hocphi.NgayDongTien).Year;
            //2. Họ và tên thuyền viên
            xlSheet.Cells[row + 1, 3] = hocphi.HocVien_Name;
            //3. Bằng cấp
            xlSheet.Cells[row + 2, 3] = hocphi.Lop_Name;
            //3. Bằng cấp
            xlSheet.Cells[row + 3, 3] = hocphi.NoiDung;
            //4. Ngày cấp

            xlSheet.Cells[row + 4, 3] = hocphi.ConLai;




            //string absolutePath = newFile.FullName;// = _environment.WebRootPath + @"\" + relativePath;

            //if (System.IO.File.Exists(absolutePath))
            //{
            //    string fileName = System.IO.Path.GetFileName(absolutePath);
            //    byte[] fileBytes = System.IO.File.ReadAllBytes(absolutePath);
            //    result = File(fileBytes, "application/x-msdownload", fileName);
            //}


        }

        //Microsoft.Office.Interop.Excel.Range mRange3;
        //mRange3 = xlSheet.Range[xlSheet.Cells[icu, 1], xlSheet.Cells[i - 1, 1]];
        //mRange3.Merge();
        //mRange3.Font.Bold = true;
        //mRange3.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //mRange3.VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;




        public void ThuHocPhi(String server, DateTime tungay, DateTime denngay)
        {



            object xlApp;
            Workbook xlBook;
            Worksheet xlSheet;
            Type t = Type.GetTypeFromProgID("Excel.Application");
            xlApp = System.Activator.CreateInstance(t);

            String link = server + "//phieuthu.xltx";
            xlBook = ((Microsoft.Office.Interop.Excel.Application)xlApp).Workbooks.Open(link);
            xlSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlBook.Worksheets.get_Item(1);
            ((Microsoft.Office.Interop.Excel.Application)xlApp).Visible = true;

            //LẤY THÔNG TIN CHI TIẾT ĐỂ IN PHIẾU THU
            var hocphi = (from a in dc.CTHocPhis
                          join b in dc.CTHocViens on a.CTHocVien_ID equals b.CTHocVien_ID
                          join c in dc.Lops on b.Lop_ID equals c.Lop_ID
                          join d in dc.HocViens on b.HocVien_ID equals d.HocVien_ID
                          where a.NgayDongTien >= tungay && a.NgayDongTien <= denngay
                          select new { d.HocVien_Name, c.Lop_Name, a.NgayDongTien, a.ConLai, a.NoiDung }).First();



            Microsoft.Office.Interop.Excel.Range mrange;
            int row = 4;
            //1. Ngày tháng
            xlSheet.Cells[row, 4] = "Ngày " + Convert.ToDateTime(hocphi.NgayDongTien).Day + " tháng " + Convert.ToDateTime(hocphi.NgayDongTien).Month + "năm " + Convert.ToDateTime(hocphi.NgayDongTien).Year;
            //2. Họ và tên thuyền viên
            xlSheet.Cells[row + 1, 3] = hocphi.HocVien_Name;
            //3. Bằng cấp
            xlSheet.Cells[row + 2, 3] = hocphi.Lop_Name;
            //3. Bằng cấp
            xlSheet.Cells[row + 3, 3] = hocphi.NoiDung;
            //4. Ngày cấp

            xlSheet.Cells[row + 4, 3] = hocphi.ConLai;




            //Microsoft.Office.Interop.Excel.Range mRange1;
            //mRange1 = xlSheet.Range[xlSheet.Cells[row, 1], xlSheet.Cells[row - 1, 6]];
            //var border_duoi = mRange1.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom];
            //border_duoi.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
            //border_duoi.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;



        }
    }
}