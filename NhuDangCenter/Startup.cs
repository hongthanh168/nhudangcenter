﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NhuDangCenter.Startup))]
namespace NhuDangCenter
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
