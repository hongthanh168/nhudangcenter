﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NhuDangCenter.Models;

namespace NhuDangCenter.Controllers
{
    public class ChungChiController : Controller
    {
        private NhuDangCenterEntities db = new NhuDangCenterEntities();

        // GET: ChungChi
        public ActionResult Index()
        {
            var hocVien_ChungChi = db.HocVien_ChungChi.Include(h => h.ChungChi).Include(h => h.HocVien);
            return View(hocVien_ChungChi.ToList());
        }

        // GET: ChungChi
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult ChungChiHocVien(int id)
        {
            var hocVien_ChungChi = db.HocVien_ChungChi.Where(h=>h.HocVien_ID==id).Include(h => h.ChungChi);
            ViewBag.HocVien_ID = id;    
            return PartialView("_ChungChiHocVien",hocVien_ChungChi.ToList());
        }

        // GET: ChungChi/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HocVien_ChungChi hocVien_ChungChi = db.HocVien_ChungChi.Find(id);
            if (hocVien_ChungChi == null)
            {
                return HttpNotFound();
            }
            return View(hocVien_ChungChi);
        }

        // GET: ChungChi/Create
        public ActionResult Create()
        {
            ViewBag.ChungChi_ID = new SelectList(db.ChungChis, "ChungChi_ID", "ChungChi_Name");
            ViewBag.HocVien_ID = new SelectList(db.HocViens, "HocVien_ID", "HocVien_Name");
            return View();
        }

        public ActionResult CreateOne(int id)
        {
            HocVien_ChungChi item = new HocVien_ChungChi();
            item.HocVien_ID = id;
            ViewBag.ChungChi_ID = new SelectList(db.ChungChis, "ChungChi_ID", "ChungChi_Name");
            ViewBag.HocVien_ID = new SelectList(db.HocViens, "HocVien_ID", "HocVien_Name");
            return View(item);
        }

        // POST: ChungChi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HocVien_ChungChi_ID,HocVien_ID,ChungChi_ID,NgayCap,DinhKem")] HocVien_ChungChi hocVien_ChungChi)
        {
            if (ModelState.IsValid)
            {
                db.HocVien_ChungChi.Add(hocVien_ChungChi);
                db.SaveChanges();
                return RedirectToAction("ChungChiHocVien", new { id = hocVien_ChungChi.HocVien_ID });
            }

            ViewBag.ChungChi_ID = new SelectList(db.ChungChis, "ChungChi_ID", "ChungChi_Name", hocVien_ChungChi.ChungChi_ID);
            ViewBag.HocVien_ID = new SelectList(db.HocViens, "HocVien_ID", "HocVien_Name", hocVien_ChungChi.HocVien_ID);
            return View(hocVien_ChungChi);
        }

        // GET: ChungChi/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HocVien_ChungChi hocVien_ChungChi = db.HocVien_ChungChi.Find(id);
            if (hocVien_ChungChi == null)
            {
                return HttpNotFound();
            }
            ViewBag.ChungChi_ID = new SelectList(db.ChungChis, "ChungChi_ID", "ChungChi_Name", hocVien_ChungChi.ChungChi_ID);
            ViewBag.HocVien_ID = new SelectList(db.HocViens, "HocVien_ID", "HocVien_Name", hocVien_ChungChi.HocVien_ID);
            return View(hocVien_ChungChi);
        }

        // POST: ChungChi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HocVien_ChungChi_ID,HocVien_ID,ChungChi_ID,NgayCap,DinhKem")] HocVien_ChungChi hocVien_ChungChi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hocVien_ChungChi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ChungChi_ID = new SelectList(db.ChungChis, "ChungChi_ID", "ChungChi_Name", hocVien_ChungChi.ChungChi_ID);
            ViewBag.HocVien_ID = new SelectList(db.HocViens, "HocVien_ID", "HocVien_Name", hocVien_ChungChi.HocVien_ID);
            return View(hocVien_ChungChi);
        }

        // GET: ChungChi/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HocVien_ChungChi hocVien_ChungChi = db.HocVien_ChungChi.Find(id);
            if (hocVien_ChungChi == null)
            {
                return HttpNotFound();
            }
            return View(hocVien_ChungChi);
        }

        // POST: ChungChi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HocVien_ChungChi hocVien_ChungChi = db.HocVien_ChungChi.Find(id);
            db.HocVien_ChungChi.Remove(hocVien_ChungChi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
