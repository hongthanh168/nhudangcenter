﻿--Thay doi store Tao bang cham cong
--Do Giao vien tro giang co the khong co nen doi lai
ALTER procedure [dbo].[sp_T_TaoBangChamCong]
	@tuNgay smalldatetime,
	@denNgay smalldatetime
as
	begin
		declare @ngay smalldatetime
		set @ngay = @tuNgay
		while @ngay< @denNgay
			begin
				--tính ra thứ của ngày
				declare @thu int
				set @thu = DATEPART(dw,@ngay)
				--lấy ra tên cột tương ứng trong bảng Schedule
				declare @sThu nvarchar(2)
				if (@thu=1) 
					set @sThu ='CN'
				else
					set @sThu = cast(@thu as varchar(1))
				--lấy ra danh sách các lớp có ngày ngày trong khoảng				
				insert into ChamCong (NgayChamCong, Lop_ID, GiaoVien_ID, KHChamCong_ID, VaiTro)
					--insert giáo viên dạy chính
					select @ngay, l.Lop_ID, l.GiaoVien_ID, 3, 1
					from Lop l, Thu t
					where l.BatDau<=@ngay and l.KetThuc>=@ngay
					and l.Thu_ID = t.Thu_ID
					and CHARINDEX(@sThu,t.CT_Thu) > 0
					union all
					--giáo viên trợ giảng
					select @ngay, l.Lop_ID, l.TroGiang_ID, 3, 0
					from Lop l, Thu t
					where l.BatDau<=@ngay and l.KetThuc>=@ngay
					and l.Thu_ID = t.Thu_ID
					and CHARINDEX(@sThu,t.CT_Thu) > 0
					and l.TroGiang_ID is not null
				set @ngay = DATEADD(day,1,@ngay)
			end
	end

--Ngay 04/05/2017
--Thay doi kieu du lieu so tiet tu int thanh decimal
alter table Ca alter column SoTiet decimal(4,2)
go
--Thay đổi bảng HRM_ROLE
alter table HRM_ROLE
add RoleFullName nvarchar(100) null
go
update HRM_ROLE
set RoleFullName =N'Quản trị'
where RoleID =1
go
update HRM_ROLE
set RoleFullName =N'Giáo viên'
where RoleID =2
go
update HRM_ROLE
set RoleFullName =N'Ghi danh'
where RoleID =3
go
--Ngày 21/4/2017
--Thay đổi bảng Chi Khác
				DROP TABLE [dbo].[ChiKhac]
				GO
				
				CREATE TABLE [dbo].[ChiKhac](
					[ChiKhac_ID] [int] IDENTITY(1,1) NOT NULL,
					[SoPhieu] [nvarchar](50) NULL,
					[Ngay] [smalldatetime] NULL,
					[SoTien] [int] NULL,
					[NoiDung] [nvarchar](200) NULL,
				 CONSTRAINT [PK_ChiKhac] PRIMARY KEY CLUSTERED 
				(
					[ChiKhac_ID] ASC
				)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
				) ON [PRIMARY]

				GO
--báo cáo thu chi theo giai đoạn
				create procedure sp_T_BaoCaoThuChi
					@tuNgay smalldatetime,
					@denNgay smalldatetime
				as
					begin
						select 'Thu' as PhanLoai, l.Lop_Name as NoiDung, sum(hp.ConLai) as ThuHocPhi, 0 as ChiKhac 
						from CTHocPhi hp, CTHocVien hv, Lop l
						where hp.NgayDongTien between @tuNgay and @denNgay
						and hp.CTHocVien_ID = hv.CTHocVien_ID
						and hv.Lop_ID = l.Lop_ID
						group by l.Lop_Name
						
						union all
						select 'Chi' as PhanLoai, NoiDung, 0 as ThuHocPhi, SoTien as ChiKhac
						from ChiKhac
						where Ngay between @tuNgay and @denNgay
					end
--Ngày 20/4/2017
--Tính lương cho giáo viên
				alter table Lop
					add LuongGiangChinh int null
				go
				alter table Lop
					add LuongTroGiang int null
				go
				CREATE TABLE [dbo].[LuongGiaoVien](
					[LuongGiaoVien_ID] [int] IDENTITY(1,1) NOT NULL,
					[NgayTraLuong] [smalldatetime] NULL,
					[Lop_ID] [int] NULL,
					[GiaoVien_ID] [int] NULL,
					[LuongGiangDay] [int] NULL,
					[Thuong] [int] NULL,
					[PhaiTru] [int] NULL,
					[GhiChu] [nvarchar](200) NULL,
				 CONSTRAINT [PK_LuongGiaoVien] PRIMARY KEY CLUSTERED 
				(
					[LuongGiaoVien_ID] ASC
				)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
				) ON [PRIMARY]

				GO

				ALTER TABLE [dbo].[LuongGiaoVien]  WITH CHECK ADD  CONSTRAINT [FK_LuongGiaoVien_GiaoVien] FOREIGN KEY([GiaoVien_ID])
				REFERENCES [dbo].[GiaoVien] ([GiaoVien_ID])
				ON UPDATE CASCADE
				ON DELETE CASCADE
				GO

				ALTER TABLE [dbo].[LuongGiaoVien] CHECK CONSTRAINT [FK_LuongGiaoVien_GiaoVien]
				GO

				ALTER TABLE [dbo].[LuongGiaoVien]  WITH CHECK ADD  CONSTRAINT [FK_LuongGiaoVien_Lop] FOREIGN KEY([Lop_ID])
				REFERENCES [dbo].[Lop] ([Lop_ID])
				GO

				ALTER TABLE [dbo].[LuongGiaoVien] CHECK CONSTRAINT [FK_LuongGiaoVien_Lop]
				GO
				------------------------------------------------------------------
				CREATE procedure [dbo].[sp_T_TinhLuongGiaoVien]
				@ngayTinhLuong smalldatetime
				as
				begin
					select l.Lop_ID, l.Lop_Name, 
						l.LuongGiangChinh, l.LuongTroGiang,
						gv.GiaoVien_ID, gv.GiaoVien_Name,
						sum(t.LuongDaTra) as LuongDaTra,
						sum(t.SoBuoiGiangChinh)*c.SoTiet as SoTietGiangChinh,
						sum(t.SoBuoiTroGiang)*c.SoTiet as SoTietTroGiang,
						(sum(t.SoBuoiGiangChinh)*c.SoTiet*l.LuongGiangChinh + sum(t.SoBuoiTroGiang)*c.SoTiet*l.LuongTroGiang) as TienPhaiTra,
						iif(sum(t.LuongDaTra)>=(sum(t.SoBuoiGiangChinh)*c.SoTiet*l.LuongGiangChinh + sum(t.SoBuoiTroGiang)*c.SoTiet*l.LuongTroGiang),
						sum(t.LuongDaTra)- (sum(t.SoBuoiGiangChinh)*c.SoTiet*l.LuongGiangChinh + sum(t.SoBuoiTroGiang)*c.SoTiet*l.LuongTroGiang), 0) as Thua,
						iif(sum(t.LuongDaTra)<=(sum(t.SoBuoiGiangChinh)*c.SoTiet*l.LuongGiangChinh + sum(t.SoBuoiTroGiang)*c.SoTiet*l.LuongTroGiang),
						(sum(t.SoBuoiGiangChinh)*c.SoTiet*l.LuongGiangChinh + sum(t.SoBuoiTroGiang)*c.SoTiet*l.LuongTroGiang) - sum(t.LuongDaTra), 0) as Thieu
					from Lop l, GiaoVien gv, Ca c, 	
						(
							select Lop_ID, GiaoVien_ID, sum(LuongGiangDay) as LuongDaTra, 0 as SoBuoiGiangChinh, 0 as SoBuoiTroGiang
							from LuongGiaoVien
							where NgayTraLuong<=@ngayTinhLuong
							group by Lop_ID, GiaoVien_ID

							union all		
							select cc.Lop_ID, cc.GiaoVien_ID, 0 as LuongDaTra, 
							count(cc.ChamCong_ID) as SoBuoiGiangChinh, 0 as SoBuoiTroGiang
							from ChamCong cc
							where cc.NgayChamCong <=@ngayTinhLuong
							and cc.KHChamCong_ID<>2 --2 là giá trị nghỉ
							and cc.VaiTro =1  --vai trò 1 là giảng chính
							group by cc.Lop_ID, cc.GiaoVien_ID

							union all		
							select cc.Lop_ID, cc.GiaoVien_ID, 0 as LuongDaTra, 
							0 as SoBuoiGiangChinh, count(cc.ChamCong_ID) as SoBuoiTroGiang
							from ChamCong cc
							where cc.NgayChamCong <=@ngayTinhLuong
							and cc.KHChamCong_ID<>2 --2 là giá trị nghỉ
							and cc.VaiTro =0 --vai trò 0 là trợ giảng
							group by cc.Lop_ID, cc.GiaoVien_ID
						) as t
					where l.Ca_ID = c.Ca_ID
					and l.Lop_ID = t.Lop_ID
					and gv.GiaoVien_ID = t.GiaoVien_ID
					group by l.Lop_ID, l.Lop_Name, l.LuongGiangChinh, l.LuongTroGiang,
						gv.GiaoVien_ID, gv.GiaoVien_Name,
						c.SoTiet
				end
				GO
--Ngày 13/4/2017
--Chỉnh sửa bảng chấm công và tạo các scipt phục vụ chấm công
					DROP TABLE [dbo].[ChamCong]
					GO

					/****** Object:  Table [dbo].[ChamCong]    Script Date: 4/14/2017 4:15:39 PM ******/
					SET ANSI_NULLS ON
					GO

					SET QUOTED_IDENTIFIER ON
					GO

					CREATE TABLE [dbo].[ChamCong](
						[ChamCong_ID] [int] IDENTITY(1,1) NOT NULL,
						[NgayChamCong] [smalldatetime] NOT NULL,
						[Lop_ID] [int] NOT NULL,
						[GiaoVien_ID] [int] NOT NULL,
						[KHChamCong_ID] [int] NULL,
						[GiaoVienDayBu_ID] [int] NULL,
						[VaiTro] [int] NULL CONSTRAINT [DF_ChamCong_VaiTro]  DEFAULT ((1)),
					 CONSTRAINT [PK_ChamCong] PRIMARY KEY CLUSTERED 
					(
						[ChamCong_ID] ASC
					)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
					) ON [PRIMARY]

					GO

					ALTER TABLE [dbo].[ChamCong]  WITH CHECK ADD  CONSTRAINT [FK_ChamCong_GiaoVien] FOREIGN KEY([GiaoVien_ID])
					REFERENCES [dbo].[GiaoVien] ([GiaoVien_ID])
					ON UPDATE CASCADE
					ON DELETE CASCADE
					GO

					ALTER TABLE [dbo].[ChamCong] CHECK CONSTRAINT [FK_ChamCong_GiaoVien]
					GO

					ALTER TABLE [dbo].[ChamCong]  WITH CHECK ADD  CONSTRAINT [FK_ChamCong_GiaoVien1] FOREIGN KEY([GiaoVienDayBu_ID])
					REFERENCES [dbo].[GiaoVien] ([GiaoVien_ID])
					GO

					ALTER TABLE [dbo].[ChamCong] CHECK CONSTRAINT [FK_ChamCong_GiaoVien1]
					GO

					ALTER TABLE [dbo].[ChamCong]  WITH CHECK ADD  CONSTRAINT [FK_ChamCong_KHChamCong] FOREIGN KEY([KHChamCong_ID])
					REFERENCES [dbo].[KHChamCong] ([KHChamCong_ID])
					GO

					ALTER TABLE [dbo].[ChamCong] CHECK CONSTRAINT [FK_ChamCong_KHChamCong]
					GO

					ALTER TABLE [dbo].[ChamCong]  WITH CHECK ADD  CONSTRAINT [FK_ChamCong_Lop] FOREIGN KEY([Lop_ID])
					REFERENCES [dbo].[Lop] ([Lop_ID])
					ON UPDATE CASCADE
					ON DELETE CASCADE
					GO

					ALTER TABLE [dbo].[ChamCong] CHECK CONSTRAINT [FK_ChamCong_Lop]
					GO

					EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: Dạy chính, 0: Dạy phụ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChamCong', @level2type=N'COLUMN',@level2name=N'VaiTro'
					GO
					--------------------------------------------------------
					CREATE procedure [dbo].[sp_T_TaoBangChamCong]
					@tuNgay smalldatetime,
					@denNgay smalldatetime
					as
					begin
						declare @ngay smalldatetime
						set @ngay = @tuNgay
						while @ngay< @denNgay
							begin
								--tính ra thứ của ngày
								declare @thu int
								set @thu = DATEPART(dw,@ngay)
								--lấy ra tên cột tương ứng trong bảng Schedule
								declare @sThu nvarchar(2)
								if (@thu=1) 
									set @sThu ='CN'
								else
									set @sThu = cast(@thu as varchar(1))
								--lấy ra danh sách các lớp có ngày ngày trong khoảng				
								insert into ChamCong (NgayChamCong, Lop_ID, GiaoVien_ID, KHChamCong_ID, VaiTro)
									--insert giáo viên dạy chính
									select @ngay, l.Lop_ID, l.GiaoVien_ID, 1, 1
									from Lop l, Thu t
									where l.BatDau<=@ngay and l.KetThuc>=@ngay
									and l.Thu_ID = t.Thu_ID
									and CHARINDEX(@sThu,t.CT_Thu) > 0
									union all
									--giáo viên trợ giảng
									select @ngay, l.Lop_ID, l.TroGiang_ID, 1, 0
									from Lop l, Thu t
									where l.BatDau<=@ngay and l.KetThuc>=@ngay
									and l.Thu_ID = t.Thu_ID
									and CHARINDEX(@sThu,t.CT_Thu) > 0
								set @ngay = DATEADD(day,1,@ngay)
							end
					end
					GO
					-----------------------------------------------------------------------
					create procedure [dbo].[sp_T_LayBangChamCongNgay]
					@ngay	smalldatetime
					as
						begin
							select gv.GiaoVien_Name, l.Lop_Name, l.Ca_ID, cc.*
							from ChamCong cc, Lop l, GiaoVien gv
							where cc.Lop_ID = l.Lop_ID
							and cc.GiaoVien_ID = gv.GiaoVien_ID
							and cc.NgayChamCong = @ngay
						end
					GO
--Ngày 02/4/2017
--Chỉnh sửa bảng học viên và tạo mã học viên
					alter table HocVien
					add MaHocVien nvarchar(6) null
					go

					alter table HocVien
					add HinhAnh image null
					go

					create FUNCTION [dbo].[fc_T_TaoMaHocVien] 
					(
						-- Add the parameters for the function here
						@nam int
					)
					RETURNS nvarchar(6)
					AS
					BEGIN
						
						Declare @kq nvarchar(6)
						declare @dau nvarchar(2)
						declare @duoi nvarchar(4)
						declare @maMax int
						set @dau= RIGHT(convert(nvarchar(4),@nam),2)
						set @maMax = (select max(convert(int, RIGHT(MaHocVien,4)))
										from HocVien
										where LEFT(MaHocVien,2)=@dau
									)
						  if (@maMax is not null)
							set @maMax = @maMax +1
						  else
							set @maMax = 1	  
						set @duoi = FORMAT(@maMax,'000#')
						set @kq = @dau+@duoi
						RETURN @kq

					END
					go
--Ngày 25/3/2017
--Tạo User Role
					SET ANSI_NULLS ON
					GO
					SET QUOTED_IDENTIFIER ON
					GO
					CREATE TABLE [dbo].[HRM_ROLE](
						[RoleID] [int] IDENTITY(1,1) NOT NULL,
						[RoleName] [nvarchar](50) NULL,
					 CONSTRAINT [PK_HRM_ROLE] PRIMARY KEY CLUSTERED 
					(
						[RoleID] ASC
					)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
					) ON [PRIMARY]

					GO
					/****** Object:  Table [dbo].[HRM_USER]    Script Date: 2/8/2017 4:48:54 PM ******/
					SET ANSI_NULLS ON
					GO
					SET QUOTED_IDENTIFIER ON
					GO
					CREATE TABLE [dbo].[HRM_USER](
						[UserID] [int] IDENTITY(1,1) NOT NULL,
						[UserName] [nvarchar](50) NULL,
						[Password] [nvarchar](500) NULL,
						[FullName] [nvarchar](50) NULL,
					 CONSTRAINT [PK_HRM_USER] PRIMARY KEY CLUSTERED 
					(
						[UserID] ASC
					)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
					) ON [PRIMARY]

					GO
					/****** Object:  Table [dbo].[HRM_USERROLES]    Script Date: 2/8/2017 4:48:54 PM ******/
					SET ANSI_NULLS ON
					GO
					SET QUOTED_IDENTIFIER ON
					GO
					CREATE TABLE [dbo].[HRM_USERROLES](
						[UserRoleID] [int] IDENTITY(1,1) NOT NULL,
						[UserID] [int] NULL,
						[RoleID] [int] NULL,
					 CONSTRAINT [PK_HRM_USERROLES] PRIMARY KEY CLUSTERED 
					(
						[UserRoleID] ASC
					)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
					) ON [PRIMARY]

					GO
					SET IDENTITY_INSERT [dbo].[HRM_ROLE] ON 

					INSERT [dbo].[HRM_ROLE] ([RoleID], [RoleName]) VALUES (1, N'Quan tri')
					INSERT [dbo].[HRM_ROLE] ([RoleID], [RoleName]) VALUES (2, N'Giao vien')
					INSERT [dbo].[HRM_ROLE] ([RoleID], [RoleName]) VALUES (3, N'Ghi danh')
					SET IDENTITY_INSERT [dbo].[HRM_ROLE] OFF
					SET IDENTITY_INSERT [dbo].[HRM_USER] ON 

					INSERT [dbo].[HRM_USER] ([UserID], [UserName], [Password], [FullName]) VALUES (1, N'hongthanh', N'e10adc3949ba59abbe56e057f20f883e', N'Phạm Thị Hồng Thanh')
					INSERT [dbo].[HRM_USER] ([UserID], [UserName], [Password], [FullName]) VALUES (2, N'phamnguyen', N'e10adc3949ba59abbe56e057f20f883e', N'Phạm Hữu Nguyên')
					INSERT [dbo].[HRM_USER] ([UserID], [UserName], [Password], [FullName]) VALUES (3, N'cattuong', N'e10adc3949ba59abbe56e057f20f883e', N'Nguyễn Phạm Cát Tường')
					SET IDENTITY_INSERT [dbo].[HRM_USER] OFF
					SET IDENTITY_INSERT [dbo].[HRM_USERROLES] ON 

					INSERT [dbo].[HRM_USERROLES] ([UserRoleID], [UserID], [RoleID]) VALUES (1, 1, 1)
					INSERT [dbo].[HRM_USERROLES] ([UserRoleID], [UserID], [RoleID]) VALUES (2, 2, 2)
					SET IDENTITY_INSERT [dbo].[HRM_USERROLES] OFF
					ALTER TABLE [dbo].[HRM_USERROLES]  WITH CHECK ADD  CONSTRAINT [FK_HRM_USERROLES_HRM_ROLE] FOREIGN KEY([RoleID])
					REFERENCES [dbo].[HRM_ROLE] ([RoleID])
					ON UPDATE CASCADE
					ON DELETE CASCADE
					GO
					ALTER TABLE [dbo].[HRM_USERROLES] CHECK CONSTRAINT [FK_HRM_USERROLES_HRM_ROLE]
					GO
					ALTER TABLE [dbo].[HRM_USERROLES]  WITH CHECK ADD  CONSTRAINT [FK_HRM_USERROLES_HRM_USER] FOREIGN KEY([UserID])
					REFERENCES [dbo].[HRM_USER] ([UserID])
					ON UPDATE CASCADE
					ON DELETE CASCADE
					GO
					ALTER TABLE [dbo].[HRM_USERROLES] CHECK CONSTRAINT [FK_HRM_USERROLES_HRM_USER]
					GO

					



