﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NhuDangCenter.Models
{
    /// <summary>
    /// Bảng ChamCong
    /// </summary>
    public class ChamCongMetadata
    {
        public int ChamCong_ID { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Ngày chấm công")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime NgayChamCong { get; set; }
        public int Lop_ID { get; set; }
        public int GiaoVien_ID { get; set; }
        public Nullable<int> KHChamCong_ID { get; set; }
        public Nullable<int> GiaoVienDayBu_ID { get; set; }
        public Nullable<int> VaiTro { get; set; }
    }
    [MetadataType(typeof(ChamCongMetadata))]
    public partial class ChamCong { }

    /// <summary>
    /// Store sp_T_TinhLuongGiaoVien
    /// </summary>

    public class sp_T_TinhLuongGiaoVien_ResultMetadata
    {
        public int Lop_ID { get; set; }
        public string Lop_Name { get; set; }
        [Display(Name = "Lương giảng chính")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public Nullable<int> LuongGiangChinh { get; set; }

        [Display(Name = "Lương trợ giảng")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public Nullable<int> LuongTroGiang { get; set; }

        public int GiaoVien_ID { get; set; }
        public string GiaoVien_Name { get; set; }

        [Display(Name = "Lương đã trả")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public Nullable<int> LuongDaTra { get; set; }

        [Display(Name = "Số tiết giảng chính")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N1}")]
        public Nullable<decimal> SoTietGiangChinh { get; set; }

        [Display(Name = "Số tiết trợ giảng")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N1}")]
        public Nullable<decimal> SoTietTroGiang { get; set; }

        [Display(Name = "Lương giảng dạy")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public Nullable<decimal> TienPhaiTra { get; set; }

        [Display(Name = "Thừa")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public Nullable<decimal> Thua { get; set; }

        [Display(Name = "Thiếu")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public Nullable<decimal> Thieu { get; set; }
    }
    [MetadataType(typeof(sp_T_TinhLuongGiaoVien_ResultMetadata))]
    public partial class sp_T_TinhLuongGiaoVien_Result { }

    /// <summary>
    /// Bảng LuongGiaoVien
    /// </summary>

    public class LuongGiaoVienMetadata
    {
        public int LuongGiaoVien_ID { get; set; }
        [Display(Name = "Ngày trả lương")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> NgayTraLuong { get; set; }
        public Nullable<int> Lop_ID { get; set; }
        public Nullable<int> GiaoVien_ID { get; set; }
        [Display(Name ="Lương giảng dạy")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public Nullable<int> LuongGiangDay { get; set; }
        [Display(Name = "Thưởng")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public Nullable<int> Thuong { get; set; }
        [Display(Name = "Phải trừ")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:N0}")]
        public Nullable<int> PhaiTru { get; set; }
        [Display(Name ="Ghi chú")]
        public string GhiChu { get; set; }
    }
    [MetadataType(typeof(LuongGiaoVienMetadata))]
    public partial class LuongGiaoVien { }

    /// <summary>
    /// Bảng ChiKhac
    /// </summary>

    public class ChiKhacMetadata
    {
        public int ChiKhac_ID { get; set; }
        [Display(Name ="Số phiếu")]
        public string SoPhieu { get; set; }

        [Display(Name = "Ngày chi")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public Nullable<System.DateTime> Ngay { get; set; }

        [Display(Name = "Số tiền")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public Nullable<int> SoTien { get; set; }

        [Display(Name ="Nội dung")]
        public string NoiDung { get; set; }
    }

    [MetadataType(typeof(ChiKhacMetadata))]
    public partial class ChiKhac{}

    /// <summary>
    /// Bảng sp_T_BaoCaoThuChi
    /// </summary>

    public class sp_T_BaoCaoThuChi_ResultMetadata
    {
        public string PhanLoai { get; set; }
        public string NoiDung { get; set; }

        [Display(Name = "Thu")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = false)]
        public Nullable<int> ThuHocPhi { get; set; }

        [Display(Name = "Chi")]
        [RegularExpression("([0-9]+)")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = false)]
        public Nullable<int> ChiKhac { get; set; }
    }

    [MetadataType(typeof(sp_T_BaoCaoThuChi_ResultMetadata))]
    public partial class sp_T_BaoCaoThuChi_Result { }

    /// <summary>
    /// Bảng HRM_USER
    /// </summary>

    public class HRM_USERMetadata
    {
        public int UserID { get; set; }
        [Display(Name ="Tên đăng nhập")]
        [Required]
        [Remote("IsExists", "HRM_USER", ErrorMessage = "Tên đăng nhập đã có")]
        public string UserName { get; set; }

        public string Password { get; set; }

        [Display(Name = "Tên đầy đủ")]
        public string FullName { get; set; }        
    }

    [MetadataType(typeof(HRM_USERMetadata))]
    public partial class HRM_USER { }

    /// <summary>
    /// Bảng Thu
    /// </summary>

    public class ThuMetadata
    {      

        public int Thu_ID { get; set; }

        [Display(Name ="Thứ trong tuần")]
        [Required(ErrorMessage ="Phải nhập giá trị này")]
        public string Thu_Name { get; set; }

        [Display(Name ="Viết tắt")]
        [Required(ErrorMessage = "Phải nhập giá trị này")]
        public string CT_Thu { get; set; }

        
    }
    [MetadataType(typeof(ThuMetadata))]
    public partial class Thu { }

    /// <summary>
    /// Bảng Ca
    /// </summary>

    public class CaMetadata
    {
        public int Ca_ID { get; set; }
        [Display(Name ="Tên ca học")]
        public string Ca_Name { get; set; }
        [Display(Name ="Số tiết")]
        [Required]
        [DisplayFormat(DataFormatString ="{0:N1}", ApplyFormatInEditMode =true)]
        public Nullable<decimal> SoTiet { get; set; }
    }
    [MetadataType(typeof(CaMetadata))]
    public partial class Ca { }

    /// <summary>
    /// Bảng CapDo
    /// </summary>

    public class CapDoMetadata
    {
        public int CapDo_ID { get; set; }
        [Display(Name ="Cấp độ")]
        public string CapDo_Name { get; set; }
        [Display(Name ="Thuộc chương trình")]
        public Nullable<int> ChuongTrinh_ID { get; set; }
    }
    [MetadataType(typeof(CapDoMetadata))]
    public partial class CapDo { }
    
    /// <summary>
    /// Bảng ChuongTrinh
    /// </summary>
    public class ChuongTrinhMetadata
    {
        public int ChuongTrinh_ID { get; set; }
        [Display(Name ="Tên chương trình")]
        public string ChuongTrinh_Name { get; set; }
    }

    [MetadataType(typeof(ChuongTrinhMetadata))]
    public partial class ChuongTrinh { }

    /// <summary>
    /// Bảng trình độ
    /// </summary>

    public class TrinhDoMetadata
    {
        public int TrinhDo_ID { get; set; }
        [Display(Name ="Trình độ")]
        public string TrinhDo_Name { get; set; }
    }
    [MetadataType(typeof(TrinhDoMetadata))]
    public partial class TrinhDo { }

}