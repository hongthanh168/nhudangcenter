﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NhuDangCenter.Models;
using System.IO;
using OfficeOpenXml;

namespace NhuDangCenter.Controllers
{
    public class LuongGiaoViensController : Controller
    {
        private NhuDangCenterEntities db = new NhuDangCenterEntities();

        // GET: LuongGiaoViens
        public ActionResult Index()
        {
            var luongGiaoViens = db.LuongGiaoViens.Include(l => l.GiaoVien).Include(l => l.Lop);
            ViewBag.BangLuong = luongGiaoViens.ToList();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "TuNgay,DenNgay")] TuNgayDenNgay obj)
        {
            var luongGiaoViens = db.LuongGiaoViens.Where(x => x.NgayTraLuong>=obj.TuNgay && x.NgayTraLuong<=obj.DenNgay).Include(l => l.GiaoVien).Include(l => l.Lop);
            ViewBag.BangLuong = luongGiaoViens.ToList();
            return View(obj);
        }
        public FileResult XuatExcel(DateTime tuNgay, DateTime denNgay)
        {
            var duLieu = db.LuongGiaoViens.Where(x => x.NgayTraLuong >= tuNgay && x.NgayTraLuong <= denNgay).Include(l => l.GiaoVien).Include(l => l.Lop);

            //đầu tiên là in tiêu đề
            string tieuDe = "Từ ngày " + tuNgay.ToString("dd/MM/yyyy") + " đến ngày " + denNgay.ToString("dd/MM/yyyy");

            FileResult result = null;
            string server = Server.MapPath("~/App_Data");

            DirectoryInfo outputDir = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments));

            FileInfo templateFile = new FileInfo(server + "//bangluong.xltx");
            FileInfo newFile = new FileInfo(outputDir.FullName + @"\bangluong.xlsx");
            if (newFile.Exists)
            {
                newFile.Delete();  // ensures we create a new workbook
                newFile = new FileInfo(outputDir.FullName + @"\bangluong.xlsx");
            }
            using (ExcelPackage package = new ExcelPackage(newFile, templateFile))
            {
                //Sheet xuất dữ liệu
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                               
                //Vị trí row sẽ insert:
                //in thống ke
                worksheet.Cells[2, 1].Value = tieuDe;
                

                int row = 4;
                int stt = 1;
                foreach (LuongGiaoVien item in duLieu)
                {
                    worksheet.Cells[row, 1].Value = stt;
                    worksheet.Cells[row, 2].Value = item.Lop.Lop_Name;
                    worksheet.Cells[row, 3].Value = item.GiaoVien.GiaoVien_Name;
                    worksheet.Cells[row, 4].Value = item.LuongGiangDay;
                    worksheet.Cells[row, 5].Value = item.Thuong;
                    worksheet.Cells[row, 6].Value = item.PhaiTru;
                    worksheet.Cells[row, 7].Value = item.GhiChu;

                    row++;
                    stt++;
                }

                //Switch the PageLayoutView back to normal
                worksheet.View.PageLayoutView = false;
                // save our new workbook and we are done!
                package.Save();


                string absolutePath = newFile.FullName;// = _environment.WebRootPath + @"\" + relativePath;

                if (System.IO.File.Exists(absolutePath))
                {
                    string fileName = System.IO.Path.GetFileName(absolutePath);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(absolutePath);
                    result = File(fileBytes, "application/x-msdownload", fileName);
                }
                return result;
            }

        }
        public ActionResult TinhLuong()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TinhLuong([Bind(Include = "Ngay")] ChonNgay obj)
        {
            if (ModelState.IsValid)
            {
                var result = db.sp_T_TinhLuongGiaoVien(obj.Ngay).ToList();
                ViewBag.BangLuong = result;
                return View(obj);
            }                     
            return View();
        }
        // GET: LuongGiaoViens/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LuongGiaoVien luongGiaoVien = db.LuongGiaoViens.Find(id);
            if (luongGiaoVien == null)
            {
                return HttpNotFound();
            }
            return View(luongGiaoVien);
        }

        // GET: LuongGiaoViens/Create
        public ActionResult Create(int? Lop_ID, int? GiaoVien_ID, int? Thua, int? Thieu, DateTime? NgayTinhLuong)
        {
            LuongGiaoVien luong = new LuongGiaoVien();
            if (NgayTinhLuong.HasValue)
            {
                luong.NgayTraLuong = NgayTinhLuong.Value;
            }
            //luong.NgayTraLuong = DateTime.Now;
            if (GiaoVien_ID.HasValue)
            {
                luong.GiaoVien_ID = GiaoVien_ID.Value;
            }
            if (Lop_ID.HasValue)
            {
                luong.Lop_ID = Lop_ID.Value;
            }
            
            if (Thieu.HasValue && Thieu >= 0)
            {
                luong.LuongGiangDay = Thieu.Value;
            }
            if (Thua.HasValue && Thua >= 0)
            {
                luong.PhaiTru = Thua.Value;
            }
            ViewBag.GiaoVien_ID = new SelectList(db.GiaoViens, "GiaoVien_ID", "GiaoVien_Name", luong.GiaoVien_ID);
            ViewBag.Lop_ID = new SelectList(db.Lops, "Lop_ID", "Lop_Name", luong.Lop_ID);
            return PartialView(luong);
        }

        // POST: LuongGiaoViens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LuongGiaoVien_ID,NgayTraLuong,Lop_ID,GiaoVien_ID,LuongGiangDay,Thuong,PhaiTru,GhiChu")] LuongGiaoVien luongGiaoVien)
        {
            if (ModelState.IsValid)
            {
                db.LuongGiaoViens.Add(luongGiaoVien);
                db.SaveChanges();
                return Json(new { success=true}, JsonRequestBehavior.AllowGet);
            }

            ViewBag.GiaoVien_ID = new SelectList(db.GiaoViens, "GiaoVien_ID", "GiaoVien_Name", luongGiaoVien.GiaoVien_ID);
            ViewBag.Lop_ID = new SelectList(db.Lops, "Lop_ID", "Lop_Name", luongGiaoVien.Lop_ID);
            return View(luongGiaoVien);
        }

        // GET: LuongGiaoViens/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LuongGiaoVien luongGiaoVien = db.LuongGiaoViens.Find(id);
            if (luongGiaoVien == null)
            {
                return HttpNotFound();
            }
            ViewBag.GiaoVien_ID = new SelectList(db.GiaoViens, "GiaoVien_ID", "GiaoVien_Name", luongGiaoVien.GiaoVien_ID);
            ViewBag.Lop_ID = new SelectList(db.Lops, "Lop_ID", "Lop_Name", luongGiaoVien.Lop_ID);
            return PartialView(luongGiaoVien);
        }

        // POST: LuongGiaoViens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LuongGiaoVien_ID,NgayTraLuong,Lop_ID,GiaoVien_ID,LuongGiangDay,Thuong,PhaiTru,GhiChu")] LuongGiaoVien luongGiaoVien)
        {
            if (ModelState.IsValid)
            {
                db.Entry(luongGiaoVien).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            ViewBag.GiaoVien_ID = new SelectList(db.GiaoViens, "GiaoVien_ID", "GiaoVien_Name", luongGiaoVien.GiaoVien_ID);
            ViewBag.Lop_ID = new SelectList(db.Lops, "Lop_ID", "Lop_Name", luongGiaoVien.Lop_ID);
            return PartialView(luongGiaoVien);
        }

        // GET: LuongGiaoViens/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LuongGiaoVien luongGiaoVien = db.LuongGiaoViens.Find(id);
            if (luongGiaoVien == null)
            {
                return HttpNotFound();
            }
            return PartialView(luongGiaoVien);
        }

        // POST: LuongGiaoViens/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LuongGiaoVien luongGiaoVien = db.LuongGiaoViens.Find(id);
            db.LuongGiaoViens.Remove(luongGiaoVien);
            db.SaveChanges();
            return Json(new { success=true}, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
